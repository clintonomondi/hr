<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/postpassword', 'HomeController@postpassword')->name('postpassword');
Route::get('/checkUser/{email}', 'HomeController@checkUser')->name('checkUser');

//employee
Route::get('/employee/register', 'EmployeeController@register')->name('register');
Route::post('/employee/register', 'EmployeeController@postUser')->name('employee/register');
Route::get('/employee/active', 'EmployeeController@getActive')->name('employee/active');
Route::get('/employee/inactive', 'EmployeeController@getInctive')->name('employee/inactive');
Route::get('/employee/edit/{id}', 'EmployeeController@edit')->name('employee/edit');
Route::post('/employee/update/{id}', 'EmployeeController@update')->name('employee/update');
Route::get('/employee/profile/{id}', 'EmployeeController@profile')->name('employee/profile');
Route::post('employee/updatePhoto/{id}', 'EmployeeController@updatePhoto')->name('employee/updatePhoto');
Route::get('employee/gallery', 'EmployeeController@gallery')->name('employee/gallery');

Route::get('/apendusers', 'ApiController@apendusers')->name('apendusers');
Route::get('/getPayslipDate', 'ApiController@getPayslipDate')->name('getPayslipDate');
Route::post('/postSearch', 'ApiController@postSearch')->name('postSearch');
Route::post('/postGalarySearch', 'ApiController@postGalarySearch')->name('postGalarySearch');

Route::get('/roles', 'RoleController@index')->name('roles');
Route::get('/roles/inactive', 'RoleController@inactive')->name('roles.inactive');
Route::post('/updateRole', 'RoleController@updateRole')->name('updateRole');
Route::post('/updateStatus', 'RoleController@updateStatus')->name('updateStatus');
Route::post('/updateStatus2', 'RoleController@updateStatus2')->name('updateStatus2');
Route::get('/report/deactivations', 'RoleController@deactivations')->name('report/deactivations');
Route::post('/report/getDeactivationByUser', 'RoleController@getDeactivationByUser')->name('report/getDeactivationByUser');

//payroll
Route::get('/payroll', 'PayrollController@index')->name('payroll/add');
Route::post('/payroll', 'PayrollController@add')->name('payroll/add');
Route::get('/deductions/all', 'PayrollController@allDeduction')->name('payroll/deduction');
Route::post('/deductions/all', 'PayrollController@allDeductionByUser')->name('payroll/allDeductionByUser');
Route::get('/deduction_logs/{id}', 'PayrollController@deduction_logs')->name('deduction_logs');

Route::get('/deductions/remove/{id}', 'PayrollController@removeDeduction')->name('deduction/remove');
Route::post('/deductions/update/{id}', 'PayrollController@updateDeduction')->name('deduction/update');
Route::post('/getIndividualPayrolldata', 'ApiController@getIndividualPayrolldata')->name('getIndividualPayrolldata');
Route::post('/getIndividualPayrolldata2', 'ApiController@getIndividualPayrolldata2')->name('getIndividualPayrolldata2');

Route::get('/leave/request', 'LeaveController@request')->name('request');
Route::get('/leave/admin/request', 'LeaveController@adminrequest')->name('admin/request');
Route::get('/admin/leaves', 'LeaveController@adminleaves')->name('admin/leaves');
Route::get('/user/leaves', 'LeaveController@myleaves')->name('user/leaves');
Route::post('/leave/request', 'LeaveController@requestPost')->name('request');
Route::post('/leave/admin/request', 'LeaveController@adminPostrequest')->name('leave/admin/request');
Route::post('/changeLeave', 'LeaveController@changeLeave')->name('changeLeave');
Route::post('/changeLeave', 'LeaveController@changeLeave')->name('changeLeave');
Route::post('/leave/recall/{id}', 'LeaveController@recall')->name('leave/recall');
Route::get('/admin/leave/view/{id}', 'LeaveController@view')->name('admin/leave/view');

//api
Route::get('/getLeaves', 'ApiController@getLeaves')->name('getLeaves');
Route::post('/checkifuserhasleave', 'ApiController@checkifuserhasleave')->name('checkifuserhasleave');
Route::post('/getLeaveInfo', 'ApiController@getLeaveInfo')->name('getLeaveInfo');
Route::get('/getData', 'ApiController@getData')->name('getData');

//paye
Route::get('/paye', 'DeductionController@paye')->name('paye');
Route::get('/nhif', 'DeductionController@nhif')->name('nhif');
Route::get('/nssf', 'DeductionController@nssf')->name('nssf');
Route::post('/addPaye', 'DeductionController@addPaye')->name('addPaye');
Route::post('/addNhif', 'DeductionController@addNhif')->name('addNhif');
Route::post('/addNssf', 'DeductionController@addNssf')->name('addNssf');
Route::get('/removePayeItem/{id}', 'DeductionController@removePayeItem')->name('removePayeItem');
Route::get('/removeNhifItem/{id}', 'DeductionController@removeNhifItem')->name('removeNhifItem');
Route::get('/removeNssfItem/{id}', 'DeductionController@removeNssfItem')->name('removeNssfItem');
Route::post('/paye/update/{id}', 'DeductionController@updatePaye')->name('updatePaye');
Route::post('/NHIF/update/{id}', 'DeductionController@updateNHIF')->name('updateNHIF');
Route::post('/NSSF/update/{id}', 'DeductionController@updateNSSF')->name('updateNSSF');

//Allowancces
Route::get('/allowances/add', 'BenefitController@index')->name('allowance/add');
Route::post('/allowance/add', 'BenefitController@add')->name('benefit/add');
Route::get('/allowance/all', 'BenefitController@all')->name('allowance/all');
Route::post('/allowance/allDeductionByUser', 'BenefitController@allDeductionByUser')->name('allowance/allDeductionByUser');

Route::get('/allowance/remove/{id}', 'BenefitController@remove')->name('allowance/remove');
Route::post('/allowance/update/{id}', 'BenefitController@update')->name('allowance/update');
Route::post('/getBenefitData', 'BenefitController@getBenefitData')->name('allowance/getBenefitData');
Route::post('/getDeductionData', 'BenefitController@getDeductionData')->name('allowance/getDeductionData');

//Payslips
Route::get('/payslip/index', 'PayslipController@index')->name('payslip/index');
Route::get('/payslip/generate', 'PayslipController@generate')->name('payslip/generate');
Route::post('/payslip/generate', 'PayslipController@push')->name('payslip/generate');
Route::get('/payslip/payslips/{date}', 'PayslipController@payslips')->name('payslip/payslips');
Route::get('/payslip/payslips/rejected/{date}', 'PayslipController@rejected')->name('payslip/rejected');
Route::get('/payslip/view/{id}', 'PayslipController@view')->name('payslip/view');
Route::post('/payslip/approve', 'PayslipController@approve')->name('payslip/approve');
Route::post('/payslip/reject', 'PayslipController@reject')->name('payslip/reject');
Route::get('/payslip/mypayslips', 'PayslipController@mypayslips')->name('payslip/mypayslips');
Route::get('/payslip/print/{batch_id}', 'PayslipController@print')->name('payslip/print');

//basic Slary
Route::get('/basic/index', 'BasicSalaryController@index')->name('basic/index');
Route::get('/basic/add', 'BasicSalaryController@add')->name('basic/add');
Route::post('/basic/add', 'BasicSalaryController@post')->name('basic/post');
Route::post('/basic/update/{id}', 'BasicSalaryController@update')->name('basic/update');


//Mainatiance
Route::get('/allowance/maintained', 'BenefitController@maintained')->name('allowance/maintained');
Route::get('/deduction/maintained', 'DeductionController@maintained')->name('deduction/maintained');
Route::post('/allowance/maintained', 'BenefitController@pushmaintained')->name('allowance/pushmaintained');
Route::post('/deduction/maintained', 'DeductionController@pushmaintained')->name('deduction/pushmaintained');
Route::post('/allowance/updatemaintained/{id}', 'BenefitController@updatemaintained')->name('allowance/updatemaintained');
Route::post('/deduction/updatemaintained/{id}', 'DeductionController@updatemaintained')->name('deduction/updatemaintained');
Route::get('/leaves/index', 'LeaveController@index')->name('leaves/index');
Route::post('/leave/pushmaintained', 'LeaveController@pushmaintained')->name('leave/pushmaintained');
Route::post('/leave/updatemaintained/{id}', 'LeaveController@updatemaintained')->name('leave/updatemaintained');

Route::get('/relief/personal', 'ReliefController@personal')->name('relief/personal');
Route::get('/payslip/preview', 'ReliefController@preview')->name('payslip/preview');
Route::post('/relief/personal', 'ReliefController@post')->name('personal/post');
Route::post('/relief/update/{id}', 'ReliefController@update')->name('personal/update');

//Reports
Route::get('/reports/paye', 'ReportController@paye')->name('report/paye');
Route::get('/reports/deduction', 'ReportController@deduction')->name('report/deduction');
Route::get('/reports/allowance', 'ReportController@allowance')->name('report/allowance');
Route::post('/reports/allowance', 'ReportController@getallowance')->name('report/allowance');
Route::post('/reports/deduction', 'ReportController@getdeduction')->name('report/deduction');
Route::get('/reports/nhif', 'ReportController@nhif')->name('report/nhif');
Route::get('/reports/nssf', 'ReportController@nssf')->name('report/nssf');
Route::post('/reports/postPayeReport', 'ReportController@postPayeReport')->name('report/postPayeReport');
Route::post('/reports/postnhifReport', 'ReportController@postnhifReport')->name('report/postnhifReport');
Route::post('/reports/postnssfReport', 'ReportController@postnssfReport')->name('report/postnssfReport');

Route::post('/sendPayslipEmail', 'PayslipController@sendPayslipEmail')->name('sendPayslipEmail');
Route::get('/payslip/userview/{id}', 'PayslipController@userview')->name('userview');


//Perfomance
Route::get('/performance/maintained', 'KPIController@maintained')->name('performance/maintained');
Route::get('/performance/performance', 'KPIController@performance')->name('performance/performance');
Route::get('/performance/myperformance', 'KPIController@myperformance')->name('performance/myperformance');
Route::post('/performance/performance', 'KPIController@performanceGetByStaff')->name('performance/bystaff');
Route::get('/performance/inactive', 'KPIController@inactive')->name('performance/inactive');
Route::post('/performance/inactive', 'KPIController@inactiveGetBy')->name('performance/inactiveGetBy');
Route::post('/performance/pushcycle', 'KPIController@pushcycle')->name('performance/pushcycle');
Route::post('/performance/record', 'KPIController@record')->name('performance/record');
Route::get('/performance/records/{id}', 'KPIController@records')->name('performance/records');
Route::get('/performance/add', 'KPIController@add')->name('performance/add');
Route::get('/performance/inactivate/{id}', 'KPIController@inactivate')->name('performance/inactivate');
Route::get('/performance/activate/{id}', 'KPIController@activate')->name('performance/activate');
Route::post('/kpa/push', 'KPIController@push')->name('kpa/push');
Route::post('/kpa/update/{id}', 'KPIController@update')->name('kpa/update');



Route::get('/password/send/otp', 'OTPController@sendotp')->name('password/send/otp');
Route::post('/password/generate/otp', 'OTPController@generateotp')->name('password/generate/otp');
Route::post('/password/verify/otp', 'OTPController@verify')->name('password/verify/otp');
Route::post('/password/set', 'OTPController@set')->name('password/set');


