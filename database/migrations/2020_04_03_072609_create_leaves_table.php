<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('leave_id');
            $table->text('reason')->nullable();
            $table->string('start_date');
            $table->string('days');
            $table->string('end_date');
            $table->string('status')->default('requested');
            $table->text('comment')->nullable();
            $table->string('employee_id');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
