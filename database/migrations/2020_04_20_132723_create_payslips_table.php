<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payslip_no');
            $table->string('month');
            $table->string('year');
            $table->string('date');
            $table->string('employee_id');
            $table->string('approved_by')->nullable();
            $table->string('created_by')->nullable();
            $table->string('batch_id');
            $table->string('status')->default('unapproved');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslips');
    }
}
