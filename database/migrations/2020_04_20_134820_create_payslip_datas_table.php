<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip_datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payslip_id');
            $table->string('type');
            $table->string('amount');
            $table->string('name');
            $table->string('month');
            $table->string('year');
            $table->string('date');
            $table->string('benefit_id')->nullable();
            $table->string('deduction_id')->nullable();
            $table->string('batch_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip_datas');
    }
}
