<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->nullable();
            $table->string('marital')->nullable();
            $table->string('dob')->nullable();
            $table->string('gender');
            $table->string('phone')->unique();
            $table->string('idno')->nullable();
            $table->string('title');
            $table->string('pin')->nullable();
            $table->string('nhif')->nullable();
            $table->string('nssf')->nullable();
            $table->string('qualification');
            $table->string('emergency')->nullable();
            $table->string('relation')->nullable();
            $table->string('type');
            $table->string('doj');
            $table->string('last_day')->nullable();
            $table->string('bank')->nullable();
            $table->string('account')->nullable();
            $table->string('status')->default('Active');
            $table->string('regno')->unique();
            $table->string('prev_emp')->nullable();
            $table->string('prev_emp_from_date')->nullable();
            $table->string('prev_emp_to_date')->nullable();
            $table->string('huduma')->nullable();
            $table->string('kin')->nullable();
            $table->string('benef_name')->nullable();
            $table->string('benef_idno')->nullable();
            $table->string('benef_phone')->nullable();
            $table->string('benef_relation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
