@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >Request for a leave</h3>
                </div>
                <div class="box-content">

                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-sm-8">
                    <form method="post" name="myForm" action="{{route('request')}}">
                        @csrf
                        @include('includes.message')
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="sel1">Leave Type:</label>
                                    <select class="form-control" id="sel1" name="leave_id" onchange="getLeaveInfo(this)">
                                        <option disabled="disabled" selected="selected">Select leave type</option>
                                        @foreach($leavess as $leave)
                                            <option value="{{$leave->id}}">{{$leave->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">START DATE</label>
                                    <input class="form-control" name="start_date" type="date" id="paye"  required>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">END DATE DATE</label>
                                    <input class="form-control" name="end_date" type="date" id="end_date"   required>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Number of days</label>
                                    <input class="form-control" name="days" type="number" id="days" disabled  required>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Reason (Optional)</label>
                                    <textarea class="form-control" rows="3" name="reason"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            @if($leaves>0)
                                <button disabled class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Request for Leave</button>&nbsp;
                                @else
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Request for Leave</button>&nbsp;

                            @endif
                            </div>
                        </div>
                    </form>

                        </div>
                        <div class="col-sm-4">
                            @if($leaves>0)
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert">&times;</button>
                                <h4>Warning!</h4>
                                <p>Sorry you are  illigible to apply for any leave.</p>
                                <p>You have {{$leaves}} under request.</p>
                            </div>
                                @else
                                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">&times;</button>
                                    <h4>Success!</h4>
                                    <p>Your are ligible to apply for another leave.</p>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
