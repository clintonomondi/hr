@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> LEAVE FOR {{$leave->employee->fname}} {{$leave->employee->lname}}</h3>

                </div>
                <div class="box-content">
                    @if($leave->status=='requested')
                    <h1 style="color: #396;">{{$leave->status}}</h1>
                        @elseif($leave->status=='accepted')
                        <h1 style="color: blue;">{{$leave->status}}</h1>
                    @elseif($leave->status=='expired')
                        <h1 style="color: orange;">{{$leave->status}}</h1>
                    @elseif($leave->status=='rejected')
                        <h1 style="color: red;">{{$leave->status}}</h1>
                        @else
                            <h1 style="color: black;">{{$leave->status}}</h1>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-sm-6">


                    <div id="clockdiv">
                        <div>
                            <span class="days"></span>
                            <div class="smalltext">Days</div>
                        </div>
                        <div>
                            <span class="hours"></span>
                            <div class="smalltext">Hours</div>
                        </div>
                        <div>
                            <span class="minutes"></span>
                            <div class="smalltext">Minutes</div>
                        </div>
                        <div>
                            <span class="seconds"></span>
                            <div class="smalltext">Seconds</div>
                        </div>

                    </div><hr>
                            <table class="table table-bordered table-hover ">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Balance</td>
                                    <td>{{$leave->balance}} Days</td>
                                </tr>
                                <tr>
                                    <td>Applied By</td>
                                    <td>{{\App\User::where('id',$leave->created_by)->pluck('name')[0]}}</td>
                                </tr>
                                <tr>
                                    <td>Approved By</td>
                                    @if(empty($leave->updated_by))
                                    <td></td>
                                    @else
                                    <td>{{\App\User::where('id',$leave->updated_by)->pluck('name')[0]}}</td>
                                        @endif
                                </tr>

                                </tbody>
                            </table>

                        </div>

                        <div class="col-sm-6">
                            <table class="table table-bordered table-hover ">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Leave Type</td>
                                    <td>{{$leave->leave->name}}</td>
                                </tr>
                                <tr>
                                    <td>Reason</td>
                                    <td>{!!$leave->reason!!}</td>
                                </tr>
                                <tr>
                                    <td>Starting from</td>
                                    <td>{{$leave->start_date}}</td>
                                </tr>
                                <tr>
                                    <td>To</td>
                                    <td>{{$leave->end_date}}</td>
                                </tr>
                                <tr>
                                    <td>Days</td>
                                    <td>{{$leave->days}}</td>
                                </tr>
                                <tr hidden>
                                    <td>Days Remaining</td>
                                    <td><p id="days">{{((strtotime($leave->end_date)-strtotime(now()))/(60 * 60 * 24))}}</p></td>
                                </tr>
                                <tr>
                                    <td>Comment</td>
                                    <td>{!!$leave->comment!!}</td>
                                </tr>
                                <tr>
                                    <td>Applied at</td>
                                    <td>{{$leave->created_at}}</td>
                                </tr>
                                <tr>
                                    <td>Approved at</td>
                                    <td>{{$leave->updated_at}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
