@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> LEAVES</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover " id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Leave Type</th>
                                    <th>Staff.</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Days</th>
                                    <th>Rem Days</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($leaves as  $key=>$leave)
                                    <tr class="show-popover" data-trigger="hover" data-placement="bottom" data-content="{!!$leave->comment!!}" data-original-title="{{$leave->status}}">
                                        <td>{{$key+1}}</td>
                                        <td>{{$leave->leave->name}}</td>
                                        <td>{{$leave->employee->fname}} {{$leave->employee->lname}}</td>
                                        <td>{{$leave->start_date}}</td>
                                        <td>{{$leave->end_date}}</td>
                                        <td>{{$leave->days}}</td>
                                        <td>{{number_format((strtotime($leave->end_date)-strtotime(now()))/(60 * 60 * 24))}}</td>
                                        <td>{{$leave->balance}}</td>
                                        @if($leave->status=='requested')
                                        <td class="bg-success">{{$leave->status}}</td>
                                            @elseif($leave->status=='accepted')
                                            <td class="bg-info">{{$leave->status}}</td>
                                        @elseif($leave->status=='rejected')
                                            <td class="bg-danger">{{$leave->status}}</td>
                                        @elseif($leave->status=='cancelled')
                                            <td class="bg-warning">{{$leave->status}}</td>
                                        @elseif($leave->status=='expired')
                                            <td class="bg-secondary">{{$leave->status}}</td>
                                            @else
                                            <td class="bg-primary">{{$leave->status}}</td>
                                        @endif
                                        @if($leave->status=='requested')
                                        <td>
                                            <a class="btn btn-success btn-sm" href="#" onclick="changeLeave('{{$leave->id}}','accepted','')"><i class="fa fa-check"></i> Accept</a>
                                            <a class="btn btn-warning btn-sm"  href="#{{$leave->id}}" role="button"  data-toggle="modal" ><i class="fa fa-ban"></i> Reject</a>
                                        </td>
                                            @elseif($leave->status=='accepted')
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="#" onclick="changeLeave('{{$leave->id}}','expired','')"><i class="fa fa-leaf"></i> Finish</a>
                                                <a class="btn btn-warning btn-sm" href="#re{{$leave->id}}" role="button"  data-toggle="modal" ><i class="fa fa-leaf"></i> Recall</a>

                                            </td>
                                            @else
                                            <td></td>
                                            @endif
                                        <td><a class="btn btn-info btn-sm" href="{{route('admin/leave/view',$leave->id)}}" ><i class="fa fa-eye"></i> View</a></td>
                                    </tr>
                                    @include('modals.rejectleave')
                                    @include('modals.recallleave')
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
