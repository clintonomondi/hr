@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> LEAVES</h3>

                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover " id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Leave Type</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Leave Days</th>
                                    <th>Remaining Days</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    <th>Date applied</th>
                                    <th>Date Accepted</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($leaves as  $key=>$leave)
                                    <tr class="show-popover" data-trigger="hover" data-placement="bottom" data-content="{!!$leave->comment!!}" data-original-title="{{$leave->status}}">
                                        <td>{{$key+1}}</td>
                                        <td>{{$leave->leave->name}}</td>
                                        <td>{{$leave->start_date}}</td>
                                        <td>{{$leave->end_date}}</td>
{{--                                        <td>{{(strtotime($leave->end_date)-strtotime($leave->start_date))/(60 * 60 * 24)}}</td>--}}
                                        <td>{{$leave->days}}</td>
                                        <td>{{number_format((strtotime($leave->end_date)-strtotime(now()))/(60 * 60 * 24))}}</td>
                                        <td>{{$leave->balance}}</td>
                                        @if($leave->status=='requested')
                                            <td class="bg-success">{{$leave->status}}</td>
                                        @elseif($leave->status=='accepted')
                                            <td class="bg-info">{{$leave->status}}</td>
                                        @elseif($leave->status=='rejected')
                                            <td class="bg-danger">{{$leave->status}}</td>
                                        @elseif($leave->status=='cancelled')
                                            <td class="bg-warning">{{$leave->status}}</td>
                                        @elseif($leave->status=='expired')
                                            <td class="bg-secondary">{{$leave->status}}</td>
                                        @else
                                            <td class="bg-primary">{{$leave->status}}</td>
                                        @endif
                                        <td>{{$leave->created_at}}</td>
                                        <td>{{$leave->updated_at}}</td>
                                        @if($leave->status=='requested')
                                            <td>
                                                <a class="btn btn-danger btn-sm" href="#" onclick="changeLeave('{{$leave->id}}','cancelled','')"><i class="fa fa-ban"></i> Cancel</a>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td><a class="btn btn-info btn-sm" href="{{route('admin/leave/view',$leave->id)}}" ><i class="fa fa-eye"></i> View</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
