@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">Request for a leave for Employee</h3>
                </div>
                <div class="box-content">

                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-sm-8">
                            <form method="post" name="myForm" action="{{route('leave/admin/request')}}">
                                @csrf
                                @include('includes.message')
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="sel1">Select user:</label>
                                            <select class="form-control" id="selUser" name="employee_id" onchange="checkifuserhasleave(this)">
                                                <option value='0'>-- Select employee --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="sel1">Leave Type:</label>
                                            <select class="form-control" id="sel1" name="leave_id" onchange="getLeaveInfo(this)">
                                                <option disabled="disabled" selected="selected">Select leave type</option>
                                                @foreach($leaves as $leave)
                                                <option value="{{$leave->id}}">{{$leave->name}}</option>
                                               @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">START DATE</label>
                                            <input class="form-control" name="start_date" type="date" id="start_date"   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">END DATE DATE</label>
                                            <input class="form-control" name="end_date" type="date" id="end_date"   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Number of days for this leave</label>
                                            <input class="form-control" name="days" type="number" id="days" disabled  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Reason (Optional)</label>
                                            <textarea class="form-control" rows="3" name="reason"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                            <button disabled id="myButton" class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Request for Leave</button>&nbsp;

                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="col-sm-4" id="leavediv">


                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
