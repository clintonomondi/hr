@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">MAINTAINED LEAVES</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    @include('modals.maitainleaves')
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button  data-toggle="modal" data-target="#allowance" class="btn btn-primary show-tooltip"  title="Add new leave"><i class="fa fa-plus"></i> Maintain Leaves</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Leave Name</th>
                            <th>Max Days</th>
                            <th>Created By</th>
                            <th>Created_at</th>
                            <th>Updated_at</th>
                            <th>Users On Leave</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($leaves as $key=>$leave)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$leave->name}}</td>
                                <td>{{$leave->days}} Days</td>
                                <td>{{$leave->user->employee->regno}}</td>
                                <td>{{$leave->created_at}}</td>
                                <td>{{$leave->updated_at}}</td>
                                <td class="text-primary">{{\App\Leave::where('leave_id',$leave->id)->where('status','accepted')->count()}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$leave->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                            @include('modals.editmaintainedleaves')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
