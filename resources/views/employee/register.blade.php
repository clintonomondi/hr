@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3 id="dataname"> Register new employee</h3>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <div id="msform">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>Personal</strong></li>
                                <li id="personal"><strong>Employment</strong></li>
                                <li id="payment"><strong>More Details</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            <form method="post" name="myForm" id="submitKasae" onsubmit="return false" enctype="multipart/form-data">
                                @csrf
                                <fieldset>
                                    <div class="form-card">
                                        <h2 class="fs-title">Personal Information</h2>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">First Name <span class="required">*</span></label>
                                                    <input class="form-control" name="fname" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter first name" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name <span class="required">*</span></label>
                                                    <input class="form-control" name="lname" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter last name" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Email address</label>
                                                    <input class="form-control"  name="email" type="text" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Marital status:</label>
                                                    <select class="form-control" id="sel1" name="marital" onkeyup="this.value = this.value.toUpperCase();">
                                                        <option disabled="disabled" selected="selected">Select marital status</option>
                                                        <option>Single</option>
                                                        <option>Married</option>
                                                        <option>Divorced</option>
                                                        <option>Widowed</option>
                                                        <option>Separated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Date of birth</label>
                                                    <input class="form-control"  type="date" placeholder="Select Date" name="dob" onkeyup="this.value = this.value.toUpperCase();">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Gender: <span class="required">*</span></label>
                                                    <select class="form-control" id="sel1" name="gender">
                                                        <option disabled="disabled" selected="selected">Select gender</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Mobile Number <span class="required">*</span></label>
                                                    <input class="form-control" name="phone" type="number" placeholder="Enter mobile number" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">ID No. <span class="required">*</span></label>
                                                    <input class="form-control" name="idno" type="text" placeholder="Enter id no" onkeyup="this.value = this.value.toUpperCase();">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="button" name="next" class="next action-button" value="Next Step" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <h2 class="fs-title">Employment Information</h2>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Job Title <span class="required">*</span></label>
                                                    <input class="form-control" name="title" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter Job Title" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">KRA PIN</label>
                                                    <input class="form-control" name="pin" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter pin" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">NHIF No. <span class="required">*</span></label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="nhif" type="text" placeholder="Enter NHIF" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">NSSF NO. <span class="required">*</span></label>
                                                    <input class="form-control" name="nssf" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter NSSF">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Highest Education Qualification: <span class="required">*</span></label>
                                                    <select class="form-control" id="sel1" name="qualification">
                                                        <option disabled="disabled" selected="selected">Select qualification</option>
                                                        <option>Doctorate</option>
                                                        <option>Masters</option>
                                                        <option>Degree</option>
                                                        <option>Diploma</option>
                                                        <option>Certification</option>
                                                        <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Emergency contact</label>
                                                    <input class="form-control" name="emergency" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter Contact" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Relation with  contact</label>
                                                    <input class="form-control" name="relation" onkeyup="this.value = this.value.toUpperCase();" type="text" placeholder="Enter relation">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sel1">Employment type: <span class="required">*</span></label>
                                                    <select class="form-control" id="sel1" name="type">
                                                        <option disabled="disabled" selected="selected">Select type</option>
                                                        <option>Permanent</option>
                                                        <option>Contract</option>
                                                        <option>Casual</option>
                                                        <option>Trainee</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <h2 class="fs-title">More Information</h2>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Huduma Number</label>
                                                    <input class="form-control" name="huduma" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter Huduma Number" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">NEXT OF KIN</label>
                                                    <input class="form-control" name="kin" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter Next of Kin" >
                                                </div>
                                            </div>
                                        </div><hr>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Beneficiary Name.</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="benef_name" type="text" placeholder="Enter Name of beneficiary" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Beneficiary ID No.</label>
                                                    <input class="form-control" name="benef_idno" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter ID NO of Beneficiary">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Beneficiary Phone</label>
                                                    <input class="form-control" name="benef_phone" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter Phone N. of Beneficiary">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Relation</label>
                                                    <input class="form-control" name="benef_relation" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="Enter relationship with beneficiary" >
                                                </div>
                                            </div>
                                        </div>

                                    </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <h2 class="fs-title">Finish</h2>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Date of joining <span class="required">*</span></label>
                                                    <input class="form-control" name="doj" type="date" placeholder="Enter date">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Basic Salary <span class="required">*</span></label>
                                                    <input class="form-control"  name="gross" type="number" value="0" placeholder="Enter Basic Salary">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Bank Name</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="bank" type="text" placeholder="Enter Bank Name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Account Number</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="account" type="number" placeholder="Enter Bank Account">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Previous Employer</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="prev_emp" type="text" placeholder="Enter Company name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Previous Employment from Date</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="prev_emp_from_date" type="date" placeholder="Select date from ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img class="img-responsive img-thumbnail" id="blah" src="{{asset('images/user.png')}}"  alt="profile picture" style="width: 50%" />
                                                <br/><br/>
                                            </div>
                                            <div class="col-md-3">
                                                <input class="form-control" name="avatar" type="file" onchange="readURL(this);" placeholder="Select image">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Previous Employment to Date</label>
                                                    <input class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="prev_emp_to_date" type="date" placeholder="Select date to ">
                                                </div>
                                            </div>
                                        </div>

                                    </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />  <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;
                                </fieldset>
{{--                                <fieldset>--}}
{{--                                    <div class="form-card">--}}
{{--                                        <h2 class="fs-title text-center">PROCEED? !</h2> <br><br>--}}
{{--                                        <div class="row justify-content-center">--}}
{{--                                            <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>--}}
{{--                                        </div> <br><br>--}}
{{--                                        <div class="row justify-content-center">--}}
{{--                                            <div class="col-7 text-center">--}}
{{--                                                <h5>Click submit button to submit</h5>--}}
{{--                                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </fieldset>--}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            </div>
        </div>





@endsection
