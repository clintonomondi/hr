@extends('layouts.app')

@section('content')

    <div class="row ">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> Edit employee  {{$user->regno}}</h3>
                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" action="{{route('postSearch')}}">
                            @csrf
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-sm" href="#"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="row ">
                        <div class="col-sm-12">

                            <div id="msform">
                                <form method="post" name="myForm" action="{{route('employee/update',$user->id)}}" onsubmit="return validateForm()">
                                    @csrf

                                        <div class="form-card">
                                            @include('includes.message')
{{--                                            <h2 class="fs-title">Personal Information</h2>--}}
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name</label>
                                                        <input class="form-control" name="fname" type="text" placeholder="Enter first name"  value="{{$user->fname}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name</label>
                                                        <input class="form-control" name="lname" type="text" placeholder="Enter last name"  value="{{$user->lname}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Email address</label>
                                                        <input class="form-control" name="email" type="text" placeholder="Enter email" value="{{$user->email}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="sel1">Marital status:</label>
                                                        <select class="form-control" id="sel1" name="marital">
                                                            <option>{{$user->marital}}</option>
                                                            <option>Single</option>
                                                            <option>Married</option>
                                                            <option>Divorced</option>
                                                            <option>Widowed</option>
                                                            <option>Separated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Date of birth</label>
                                                        <input class="form-control"  type="date" placeholder="Select Date" name="dob" value="{{$user->dob}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="sel1">Gender:</label>
                                                        <select class="form-control" id="sel1" name="gender">
                                                            <option>{{$user->gender}}</option>
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Mobile Number</label>
                                                        <input class="form-control" name="phone" type="number" placeholder="Enter mobile number" value="{{$user->phone}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">ID No.</label>
                                                        <input class="form-control" name="idno" type="text" placeholder="Enter id no" value="{{$user->idno}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Job Title</label>
                                                        <input class="form-control" name="title" type="text" placeholder="Enter Job Title" value="{{$user->title}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <hr>
                                        <div class="form-card">

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">KRA PIN</label>
                                                        <input class="form-control" name="pin" type="text" placeholder="Enter pin" value="{{$user->pin}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">NHIF No.</label>
                                                        <input class="form-control" name="nhif" type="text" placeholder="Enter NHIF" value="{{$user->nhif}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">NSSF NO.</label>
                                                        <input class="form-control" name="nssf" type="text" placeholder="Enter NSSF" value="{{$user->nssf}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="sel1">Highest Education Qualification:</label>
                                                        <select class="form-control" id="sel1" name="qualification">
                                                            <option>{{$user->qualification}}</option>
                                                            <option>Doctorate</option>
                                                            <option>Masters</option>
                                                            <option>Degree</option>
                                                            <option>Diploma</option>
                                                            <option>Certification</option>
                                                            <option>Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Emergency contact</label>
                                                        <input class="form-control" name="emergency" type="text" placeholder="Enter Contact" value="{{$user->emergency}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Relation with  contact</label>
                                                        <input class="form-control" name="relation" type="text" placeholder="Enter relation" value="{{$user->relation}}">
                                                    </div>
                                                </div>
                                            </div>

                                     <hr>

                                        <div class="form-card">
{{--                                            <h2 class="fs-title">More Information</h2>--}}
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="sel1">Employment type:</label>
                                                        <select class="form-control" id="sel1" name="type">
                                                            <option>{{$user->type}}</option>
                                                            <option>Permanent</option>
                                                            <option>Contract</option>
                                                            <option>Casual</option>
                                                            <option>Trainee</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Date of joining</label>
                                                        <input class="form-control" name="doj" type="date" placeholder="Enter date" value="{{$user->doj}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Last day of working(NA)</label>
                                                        <input class="form-control" name="last_day" type="date" placeholder="Enter date" value="{{$user->last_day}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Bank Name</label>
                                                        <input class="form-control" name="bank" type="text" placeholder="Enter Bank Name" value="{{$user->bank}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Number</label>
                                                        <input class="form-control" name="account" type="number" placeholder="Enter Bank Account" value="{{$user->account}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Status</label>
                                                        <input class="form-control" name="status" type="text"  value="{{$user->status}}" disabled>
                                                    </div>
                                                </div>
                                            </div>


                                         <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;

                                        </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
