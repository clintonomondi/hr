@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> Employee Profile:  {{$user->status}}</h3>
                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" action="{{route('postSearch')}}">
                            @csrf
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-sm" href="#"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-12">
                            <div class="box-content">

                                <div class="container emp-profile" style="background-color: #F5F5DC">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <form method="post"  action="{{route('employee/updatePhoto',$user->id)}}"  enctype="multipart/form-data">
                                                                                            @csrf

                                                                                        <div class="card gallary_card mb-5 show-tooltip" data-placement="top" data-original-title="Click the image to view original image">
                                                                                            @if(empty($user->photo->avatars))
                                                                                                <a class="example-image-link" href="{{asset('images/user.png')}}g" data-lightbox="example-1"><img id="blah"  class="card__img"  alt="{{$user->fname}} {{$user->lname}}"  src="{{asset('images/user.png')}}"></a>
                                                                                            @else
                                                                                                <a class="example-image-link" href="/storage/avatars/{{$user->photo->avatars}}" data-lightbox="example-1">  <img id="blah"  class="card__img"  alt="{{$user->fname}} {{$user->lname}}"  src="/storage/avatars/{{$user->photo->avatars}}"></a>
                                                                                            @endif
                                                                                            <div class="card__content">
                                                                                                <h3 class="card__header">{{$user->fname}} {{$user->lname}}</h3>
                                                                                                <span class="fa fa-envelope">__{{$user->email}}</span><br><br>
                                                                                                <span class="fa fa-phone">__{{$user->phone}}</span><br>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <input class="form-control" name="avatar" type="file" onchange="readURL(this);" placeholder="Select image">
                                                                                                    </div>
                                                                                                    <div class="col-md-6">
                                                                                                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-upload"></i>Update</button>&nbsp;
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        </form>
                                            </div>
                                            <div class="col-md-6" style="background-color: white;">
                                                <div class="profile-head">
                                                    <h5>
                                                        {{$user->fname}} {{$user->lname}}
                                                    </h5>
                                                    <h6>
                                                        {{$user->user->role}}
                                                    </h6>
                                                    <p class="proile-rating">Staff Number : <span>{{$user->regno}}</span></p>

                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <ul class="nav nav-tabs">
                                                                <li class="active"><a data-toggle="tab" href="#home">Personal Info</a></li>
                                                                <li><a data-toggle="tab" href="#menu1">Employment</a></li>
                                                                <li><a data-toggle="tab" href="#menu2">More Details</a></li>
                                                                <li><a data-toggle="tab" href="#menu3">Other Details</a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div id="home" class="tab-pane fade in active">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>First name</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->fname}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Last name</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->lname}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Email</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->email}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Marital Status</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->marital}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Date of Birth</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->dob}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Gender</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->gender}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Mobile Number</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->phone}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>ID Number</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->idno}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="menu1" class="tab-pane fade">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Job Title</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->title}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>KRA Pin</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->pin}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>NHIF</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->nhif}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>NSSF</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->nssf}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Higher Education</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->qualification}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Emergency Contact</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->emergency}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Relationship with contact</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->relation}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Employment Type</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->type}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="menu2" class="tab-pane fade">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Date of Joining</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->doj}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Basic Salary</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">KSh. {{number_format($user->payroll->gross,2)}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Bank Name</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->bank}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Account Number</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->account}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Previous Employer</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->prev_emp}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Previous employment from</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->prev_empfrom_date}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Previous employment to</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->prev_emp_to_date}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="menu3" class="tab-pane fade">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Huduma Number</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->huduma}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Next of Kin</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->kin}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Beneficiary name</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->benef_name}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Beneficiary ID</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->idno}}benef_idno</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Beneficiary Phone</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->benef_phone}}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Relation</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="profile-p">{{$user->benf_relation}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <a  href="{{route('employee/edit',$user->id)}}" class="btn profile-edit-btn">Edit Profile</a>
                                            </div>
                                        </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div id="myModal" class="modal myModal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

@endsection
