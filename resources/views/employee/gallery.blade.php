@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> Active employee</h3>

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" action="{{route('postGalarySearch')}}">
                            @csrf
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="postGalarySearch()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <ul class="nav nav-tabs nav-justified">
                        <li ><a href="{{route('employee/active')}}">Active Employees</a></li>
                        <li><a href="{{route('employee/inactive')}}">InActive Employees</a></li>
                        <li class="active"><a href="{{route('employee/gallery')}}">Gallery</a></li>
                    </ul>

                    <!-- BEGIN Main Content -->
                    <div class="row">
                        <div class="col-sm-12">
                                    <!-- Portfolio Gallery Grid -->
                                    <div class="row">
                                        @foreach($users as $key=>$user)
                                            <div class="col-sm-3">
                                                   <div class="card gallary_card mb-5 show-tooltip" data-placement="top" data-original-title="Click the image to view original image">
                                                       <a class="example-image-link" href="/storage/avatars/{{$user->photo->avatars}}" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                                                           <img id="myImg{{$user->id}}" class="card__img"  alt="{{$user->fname}} {{$user->lname}}"  src="/storage/avatars/{{$user->photo->avatars}}"></a>

                                                       <div class="card__content">
                                                           <h3 class="card__header">{{$user->fname}} {{$user->lname}}</h3>
                                                          <span class="fa fa-envelope">__{{$user->email}}</span><br><br>
                                                          <span class="fa fa-phone">__{{$user->phone}}</span><br>
                                                         <a class="card__btn text-info" href="{{route('employee/profile',$user->id)}}">Explore<span>&rarr;</span></a>
                                                       </div>
                                                   </div>
                                               </div>
                                        @endforeach

                                        <!-- END GRID -->
                                    </div>

                                    <div class="text-center">
                                        <ul class="pagination pagination-bullet">
                                            {{ $users->links() }}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Main Content -->


                </div>

            </div>
    <div id="myModal" class="modal myModal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

@endsection
