@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname"> Active employees</h3>
                    <div class="row justify-content-center">
                        <form method="post" action="{{route('postSearch')}}">
                            @csrf
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-sm" href="#"><i class="fa fa-search"></i> Search</button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="box-content">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="{{route('employee/active')}}">Active Employees</a></li>
                        <li><a href="{{route('employee/inactive')}}">InActive Employees</a></li>
                        <li><a href="{{route('employee/gallery')}}">Gallery</a></li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12 mx-0">
<table class="table  table-bordered table-hover table-striped" id="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Staff No.</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Title</th>
        <th>Gender</th>
        <th>Date of joining</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as  $key=>$user)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$user->fname}}  {{$user->lname}}</td>
            <td>{{$user->regno}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->title}}</td>
            <td>{{$user->gender}}</td>
            <td>{{$user->doj}}</td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{route('employee/edit',$user->id)}}"><i class="fa fa-edit"></i> Edit</a>
                <a class="btn btn-info btn-sm" href="{{route('employee/profile',$user->id)}}"><i class="fa fa-eye"></i> More</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>



@endsection
