@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">Individual Payroll Allowance</h3>
                </div>
                <div class="box-content">
                    <p class="text-danger text-center"><strong></strong>**Please note that: Similar allowance will be skipped if it has not expired**</p>
                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                    <form method="post"  action="{{route('benefit/add')}}">
                        @include('includes.message')
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
{{--                                    <label class="control-label">EMPLOYEE NAME</label>--}}
                                    <select class="form-control" id="selUser" name="employee_id">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="align-right">
                                        <select class="form-control" id="sel1" name="role" onchange="addBenefit(this.value)" required>
                                             <option disabled="disabled" selected="selected">Select allowance</option>
                                            @foreach($benefits as $benefit)
                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                                                @endforeach
                                          </select>
                                        &nbsp; </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div id="deductionForm">


                                </div>
                            </div>
                        </div><hr>
                        <div class="container">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>SAVE</button>&nbsp;
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
        </div>
    </div>

@endsection
