@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(empty($user))
                        <h3 id="dataname" >Allowances</h3>
                    @else
                        <h3 id="dataname" >Allowances For {{$user->fname}}  {{$user->regno}}</h3>
                    @endif

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('allowance/allDeductionByUser')}}">
                            @csrf
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="align-right">
                                        <select class="form-control" id="sel1" name="allowance_id"  onchange="submitform()">
                                             <option disabled="disabled" selected="selected">Select Allowance</option>
                                            @foreach($allowances as $benefit)
                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                                            @endforeach
                                          </select>
                                        &nbsp; </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('includes.message')
                    <div class="clearfix">


                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('allowance/add')}}" class="btn btn-primary show-tooltip"  title="Add new Allowance"><i class="fa fa-plus"></i> Add Allowance</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Employee Name</th>
                            <th>Staff No.</th>
                            <th>Allowance Name</th>
                            <th>Amount</th>
                            <th>Frequency(months)</th>
                            <th>Remaining Months</th>
                            <th>Status</th>
                            <th>Date created</th>
                            <th>Added by</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($benefits as $key=>$benefit)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$benefit->employee->fname}} {{$benefit->employee->lname}}</td>
                                <td>{{$benefit->employee->regno}}</td>
                                <td>{{$benefit->allowance->name}}</td>
                                <td>{{number_format($benefit->amount,2)}}</td>
                                <td>{{$benefit->frequency}}</td>
                                <td>{{$benefit->months}}</td>
                                @if($benefit->months>0)
                                    <td class="text-success">ACTIVE</td>
                                    @else
                                    <td class="text-danger">EXPIRED</td>
                                    @endif
                                <td>{{$benefit->date}}</td>
                                <td>{{$benefit->user->employee->regno}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$benefit->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#b{{$benefit->id}}" href="#"><i class="fa fa-eye"></i> Remove</a>
                                </td>
                            </tr>
@include('modals.removeallowance')
@include('modals.editallowance')

                            @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
