@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(empty($user))
                    <h3 id="dataname">KPI PERFORMANCE</h3>
                        @else
                        <h3 id="dataname">KPI PERFORMANCE  FOR {{$user->fname}} {{$user->regno}}</h3>
                    @endif

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('performance/bystaff')}}">
                            @csrf
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control"  name="kpi_id" onchange="submitform()">
                                        <option disabled="disabled" selected="selected">Select KPI</option>
                                        @foreach($indicators as $kpi)
                                            <option value='{{$kpi->id}}'>{{$kpi->kpi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </form>
                    </div>
{{--                    @include('includes.message')--}}
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a  href="{{route('performance/add')}}" class="btn btn-primary show-tooltip"  title="Add new KPI"><i class="fa fa-plus"></i> Add Cycle</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="{{route('performance/performance')}}">Active Cycle</a></li>
                        <li><a href="{{route('performance/inactive')}}">Previous Cycles</a></li>
                    </ul>
                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>KPI</th>
                            <th>Staff</th>
                            <th>Staff No</th>
                            <th>Target</th>
                            <th>Achieved</th>
                            <th>%</th>
                            <th>Comment</th>
                            <th>Period from</th>
                            <th>Period To</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kpis as $key=>$kpi)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$kpi->kpi->kpi}}</td>
                                <td>{{$kpi->employee->fname}} {{$kpi->employee->lname}}</td>
                                <td>{{$kpi->employee->regno}}</td>
                                <td>{{$kpi->target}}</td>
                                <td>{{$kpi->performance_data->sum('unit')}}</td>
                                @if($kpi->target>0)
                                <td>{{number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)}}%</td>
                                @if(($kpi->performance_data->count())>=3)
                                @if(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=90)
                                    <td class="text-success">EXCELLENT</td>
                                    @elseif(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=70 && number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)<=89)
                                    <td class="text-info">GOOD</td>
                                    @elseif(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=50 && number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)<=69)
                                    <td class="text-warning">POOR</td>
                                @else
                                    <td class="text-danger">UNACCEPTABLE</td>
                                    @endif
                                @else
                                    <td></td>
                                @endif
                                @else
                                    <td></td>
                                    <td></td>
                                @endif


                                <td>{{$kpi->from}}</td>
                                <td>{{$kpi->to}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-cog"></i> Action <span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-info">
                                            <li><a data-toggle="modal" data-target="#e{{$kpi->id}}" href="#">Record</a></li>
                                            <li><a href="{{route('performance/inactivate',$kpi->id)}}">Inactivate</a></li>
                                            <li><a href="{{route('performance/records',$kpi->id)}}">View Records</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @include('modals.recordperfomance')
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
