@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">MAINTAINED KPI</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    @include('modals.maintainkpi')
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button  data-toggle="modal" data-target="#allowance" class="btn btn-primary show-tooltip"  title="Add new KPI"><i class="fa fa-plus"></i> Maintain KPI</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>KPI</th>
                            <th>Unit of Measure</th>
                            <th>Target</th>
                            <th>Created By</th>
                            <th>Created_at</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kpis as $key=>$kpi)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$kpi->kpi}}</td>
                                <td>{{$kpi->unit}}</td>
                                <td>{{$kpi->target}}</td>
                                <td>{{$kpi->user->name}}</td>
                                <td>{{$kpi->created_at}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$kpi->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                            @include('modals.editmaintainedkpi')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
