@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">Add KPI Cycle</h3>

                </div>
                <div class="box-content">
                    <div class="clearfix">
                        <div class="pull-left">
                            <a class="btn btn-info btn-sm" href="{{route('performance/performance')}}"><i class="fa fa-backward ml-5"></i>Back to Summery</a>
                        </div>
                    </div>

                    <form method="post" name="myForm" action="{{route('performance/pushcycle')}}" onsubmit="loadbutton()">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="sel1">Select user:</label>
                                    <select class="form-control" id="selUser" name="employee_id" >
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="sel1">Select KPI:</label>
                                    <select class="form-control"  name="kpi_id" required>
                                        <option disabled="disabled" selected="selected">Select KPI</option>
                                        @foreach($kpis as $kpi)
                                            <option value='{{$kpi->id}}'>{{$kpi->kpi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">DATE FROM</label>
                                    <input class="form-control" name="from" type="date" id="from"   required>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label"> DATE TO</label>
                                    <input class="form-control" name="to" type="date" id="to"   required>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label"> TARGET (Unit can be a mount or a number)</label>
                                    <input class="form-control" name="target" type="number" id="to"   required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button id="submit"   class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;

                            </div>
                        </div>
                    </form>

            </div>
        </div>

    </div>
    </div>
@endsection
