@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                        <h3 id="dataname"> MY KPI PERFORMANCE</h3>
                </div>
                <div class="box-content">
                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>KPI</th>
                            <th>Staff</th>
                            <th>Staff No</th>
                            <th>Target</th>
                            <th>Achieved</th>
                            <th>%</th>
                            <th>Comment</th>
                            <th>Period from</th>
                            <th>Period To</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kpis as $key=>$kpi)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$kpi->kpi->kpi}}</td>
                                <td>{{$kpi->employee->fname}} {{$kpi->employee->lname}}</td>
                                <td>{{$kpi->employee->regno}}</td>
                                <td>{{$kpi->target}}</td>
                                <td>{{$kpi->performance_data->sum('unit')}}</td>
                                @if($kpi->target>0)
                                    <td>{{number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)}}%</td>
                                    @if(($kpi->performance_data->count())>=3)
                                        @if(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=90)
                                            <td class="text-success">EXCELLENT</td>
                                        @elseif(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=70 && number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)<=89)
                                            <td class="text-info">GOOD</td>
                                        @elseif(number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)>=50 && number_format(($kpi->performance_data->sum('unit')/$kpi->target)*100)<=69)
                                            <td class="text-warning">POOR</td>
                                        @else
                                            <td class="text-danger">UNACCEPTABLE</td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                @else
                                    <td></td>
                                    <td></td>
                                @endif


                                <td>{{$kpi->from}}</td>
                                <td>{{$kpi->to}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle"><i class="fa fa-cog"></i> Action <span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-info">
                                            <li><a href="{{route('performance/records',$kpi->id)}}">View Records</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
