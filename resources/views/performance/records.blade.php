@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">{{$data->kpi->kpi}}  ( From {{$data->from}}   To {{$data->from}})</h3>
                    <p class="text-center">Staff : {{$data->employee->fname}} {{$data->employee->lname}}</p>

                </div>
                <div class="box-content">
                    <div class="clearfix">
                        <div class="pull-left">
                            @cannot('isUser')
                            <a class="btn btn-info btn-sm" href="{{route('performance/performance')}}"><i class="fa fa-backward ml-5"></i>Back to Performance</a>
                       @endcannot
                        </div>
                    </div>
                    @include('includes.message')

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>KPI</th>
                            <th>Duration</th>
                            <th>Unit of Measure</th>
                            <th>Weight</th>
                            <th>Target</th>
                            <th scope="col" colspan="{{$kpis->count()}}">Achievement</th>
                            <th>Score</th>
                            <th>Variance</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                                @foreach($kpis as $key=>$kpi)
                                <td class="text-success"> Week {{$kpi->week}}</td>
                                @endforeach
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{{$data->kpi->kpi}}</td>
                            <td>Weekly</td>
                            <td>{{$data->kpi->unit}}</td>
                            <td>{{$data->performance_data->sum('unit')}}</td>
                            <td>{{$data->target}}</td>
                            @foreach($kpis as $key=>$kpi)
                                <td>  {{$kpi->unit}}</td>
                            @endforeach
                            @if($data->target>0)
                            <td>{{number_format(($data->performance_data->sum('unit')/$data->target)*100)}}%</td>
                            @else
                                <td></td>
                                @endif
                            <td></td>
                            <td></td>
                        </tr>


                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
