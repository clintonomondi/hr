@if(count($errors)>0)
    @foreach($errors->all()  as $error)

            <p class="text-danger"> {{$error}}</p>

    @endforeach
@endif

@if(session('success'))

        <p class="text-success">  {{session('success')}}</p>

@endif
@if(session('error'))

        <p class="text-danger">  {{session('error')}}</p>


@endif

