@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >MAINTAINED ALLOWANCES</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    @include('modals.maintainallowance')
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button  data-toggle="modal" data-target="#allowance" class="btn btn-primary show-tooltip"  title="Add new Allowance"><i class="fa fa-plus"></i> Maintain Allowance</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Allowance Name</th>
                            <th>Created_at</th>
                            <th>Updated_at</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allowances as $key=>$allowance)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$allowance->name}}</td>
                                <td>{{$allowance->created_at}}</td>
                                <td>{{$allowance->updated_at}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$allowance->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                            @include('modals.editmaintainedallowance')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
