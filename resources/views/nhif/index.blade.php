@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >NHIF DEDUCTIONS</h3>
                </div>
                <div class="box-content">
                    <span class="align-right"> <button class="btn btn-info" onclick="addnHIFrange()" type="submit"><i class="fa fa-fw fa-lg fa-plus"></i>Add range</button>&nbsp; </span><hr>
                    {{--                    @include('includes.message')--}}
                    <div class="row">
                        @include('includes.message')
                        <div class="col-sm-7">

                            <form method="post" name="myForm"  action="{{route('addNhif')}}">
                                @csrf
                                <div id="payerform">


                                </div>
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>SAVE</button>&nbsp;
                            </form>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped table-hover table-bordered" id="table">
                                <thead>
                                <th>#</th>
                                <th>Amount from</th>
                                <th>Amount to</th>
                                <th>Amount deducted</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($nhifs as $key=>$paye)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{number_format($paye->from_amount,2)}}</td>
                                        <td>{{number_format($paye->to_amount,2)}}</td>
                                        <td>{{number_format($paye->amount,2)}}</td>
                                        <td> <a data-toggle="modal" data-target="#edit{{$paye->id}}"  class="btn btn-info btn-sm"><i class="fa fa-edit">Edit</i></a></td>
                                        <td> <a data-toggle="modal" data-target="#remove{{$paye->id}}"  class="btn btn-danger btn-sm"><i class="fa fa-ban">Remove</i></a></td>
                                    </tr>
                                    @include('modals.editnhif')
                                    @include('modals.confirmRemoveNhif')
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


@endsection
