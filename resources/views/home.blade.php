@extends('layouts.app')

@section('content')
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
        <ul class="breadcrumb">
            <li class="active"><i class="fa fa-home"></i> Home</li>
        </ul>
    </div>
    <!-- END Breadcrumb -->


    <!-- BEGIN Tiles -->

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-content">
@include('includes.message')
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tile">
                                <p class="title">Welcome {{Auth()->user()->name}}</p>
                                <p>You have logged in as {{Auth()->user()->role}}.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-location-arrow"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12 tile-active">
                            <div class="tile tile-magenta">
                                <div class="img img-center">
                                    <i class="fa fa-users"></i>
                                </div>
                                <p class="title text-center">Active Users  {{$activeusers}}</p>
                            </div>

                            <div class="tile tile-blue">
                                <p class="title">Inactive Users</p>
                                <p>{{$inactiveusers}} Are not able to login.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-user-md"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6">
                    <div class="tile tile-orange">
                        <div class="img">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="content">
                            <p class="big">{{$leaves}}</p>
                            <p class="title">Pending Leaves</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="tile tile-dark-blue">
                        <div class="img">
                            <i class="fa fa-download"></i>
                        </div>
                        <div class="content">
                            <p class="big">+160</p>
                            <p class="title">Downloads</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><hr>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-title">
                                        <h3 id="dataname"><i class="fa fa-bar-chart-o"></i> Gender</h3>
                                    </div>
                                    <div class="box-content">

                                        <div>
                                            <canvas id="pieChart" style="max-width: 500px;"></canvas>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="box">
                                    <div class="box-title">
                                        <h3 id="dataname"><i class="fa fa-bar-chart-o"></i> Allowance Over Deductions {{now()->year}}</h3>
                                    </div>
                                    <div class="box-content">

                                   <div>
                                       <canvas id="lineChart"></canvas>
                                   </div>

                                    </div>
                                </div>
                            </div>


                    </div>

                </div>
            </div>
        </div>
    </div>




    <!-- END Tiles -->
@endsection
