@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >Individual Payroll Deductions</h3>
                </div>
                <div class="box-content">

                    <hr>
                    <form method="post" name="myForm" action="{{route('payroll/add')}}" onsubmit="return validatePayrollForm()">
                        @csrf
                        @include('includes.message')
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {{--                                    <label class="control-label">EMPLOYEE NAME</label>--}}
                                    <select class="form-control" id="selUser" name="employee_id">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="align-right">
                                        <select class="form-control" id="sel1" name="role" onchange="addDeduction(this.value)" required>
                                             <option disabled="disabled" selected="selected">Select Deduction</option>
                                            @foreach($benefits as $benefit)
                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                                            @endforeach
                                          </select>
                                        &nbsp; </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                        <div id="deductionForm">


                        </div>
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>SAVE</button>&nbsp;
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
