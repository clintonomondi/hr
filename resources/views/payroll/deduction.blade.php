@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(empty($user))
                        <h3 id="dataname" >Deductions</h3>
                    @else
                        <h3 id="dataname" >Deductions For {{$user->fname}}  {{$user->regno}}</h3>
                    @endif

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('payroll/allDeductionByUser')}}">
                            @csrf
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="align-right">
                                        <select class="form-control" id="sel1" name="deduction_id"  onchange="submitform()">
                                             <option disabled="disabled" selected="selected">Select Deduction</option>
                                            @foreach($allowances as $benefit)
                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                                            @endforeach
                                          </select>
                                        &nbsp; </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('includes.message')
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('payroll/add')}}" class="btn btn-primary show-tooltip"  title="Add new Allowance"><i class="fa fa-plus"></i> Add Deductions</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Staff No.</th>
                        <th>Deduction name</th>
                        <th>Total Amount</th>
                        <th>Frequency(Months)</th>
                        <th>Monthly Deduction</th>
                        <th>Balance</th>
                        <th>Expiry</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($deductions as $key=>$deduction)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$deduction->employee->fname}} {{$deduction->employee->lname}}</td>
                                <td>{{$deduction->employee->regno}}</td>
                                <td>{{$deduction->deduction->name}}</td>
                                <td>{{number_format($deduction->amount,2)}}</td>
                                <td>{{$deduction->frequency}}  Months</td>
                                <td>{{number_format($deduction->amount_deducted,2)}}</td>
                                <td>{{number_format($deduction->balance,2)}}</td>
                                <td>{{$deduction->expiry}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$deduction->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#b{{$deduction->id}}" href="#"><i class="fa fa-eye"></i> Remove</a>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-sm"  href="{{route('deduction_logs',$deduction->id)}}"><i class="fa fa-file"></i> Logs</a>
                                </td>
                            </tr>
                            @include('modals.removededuction')
                            @include('modals.editdeduction')

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
