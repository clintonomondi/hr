@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                        <h3 id="dataname" >Deductions Logs</h3>
                </div>
                <div class="box-content">
{{--                    <div class="row justify-content-center">--}}
{{--                        <form method="post" id="reportform" name="reportform" action="{{route('payroll/allDeductionByUser')}}">--}}
{{--                            @csrf--}}
{{--                            <div class="col-sm-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">--}}
{{--                                        <option value='0'>-- Select employee --</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <span class="align-right">--}}
{{--                                        <select class="form-control" id="sel1" name="deduction_id"  onchange="submitform()">--}}
{{--                                             <option disabled="disabled" selected="selected">Select Deduction</option>--}}
{{--                                            @foreach($allowances as $benefit)--}}
{{--                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>--}}
{{--                                            @endforeach--}}
{{--                                          </select>--}}
{{--                                        &nbsp; </span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
                    @include('includes.message')
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('payroll/deduction')}}" class="btn btn-primary show-tooltip"  title="Add new Allowance"><i class="fa fa-backward"></i> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <th>#</th>
                        <th>Deduction Name</th>
                        <th>Staff Name</th>
                        <th>Staff No.</th>
                        <th>Payslip</th>
                        <th>Amount Deducted</th>
                        <th>Batch</th>
                        <th>Date</th>
                        </thead>
                        <tbody>
                        @foreach($logs as $key=>$deduction)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$deduction->deduction->name}}</td>
                                <td>{{$deduction->employee->fname}} {{$deduction->employee->lname}}</td>
                                <td>{{$deduction->employee->regno}}</td>
                                <td>{{$deduction->payslip->payslip_no}}</td>
                                <td>{{number_format($deduction->amount,2)}}</td>
                                <td>{{$deduction->batch_id}} </td>
                                <td>{{$deduction->date}}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
