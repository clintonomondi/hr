@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">BASIC SALARY</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    <div class="clearfix">
                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('basic/add')}}" class="btn btn-primary show-tooltip"  title="Add new Basic"><i class="fa fa-plus"></i> Add Basic</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Employee Name</th>
                            <th>Staff No.</th>
                            <th>Title</th>
                            <th>Basic Salary (Monthly)</th>
                            <th>Date created</th>
                            <th>Date updated</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($basics as $key=>$basic)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$basic->employee->fname}} {{$basic->employee->lname}}</td>
                                <td>{{$basic->employee->regno}}</td>
                                <td>{{$basic->employee->title}}</td>
                                <td>{{number_format($basic->gross,2)}}</td>
                                <td>{{$basic->created_at}}</td>
                                <td>{{$basic->updated_at}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#e{{$basic->id}}" href="#"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                            @include('modals.editbasic')


                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection
