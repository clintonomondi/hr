@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">BASIC SALARY</h3>

                </div>
                <div class="box-content">
                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('basic/index')}}" class="btn btn-primary show-tooltip"  title="Basic Salaries"><i class="fa fa-plus"></i> Basic Salary</a>
                                </div>
                            </div>
                        </div>
                    </div><hr>

                    <div class="row justify-content-center">
                        <div class="col-sm-8">
                            <form method="post" name="myForm" action="{{route('basic/post')}}">
                                @csrf
                                @include('includes.message')
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="sel1">Select user:</label>
                                            <select class="form-control" id="selUser" name="employee_id">
                                                <option value='0'>-- Select employee --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">BASIC SALARY AMOUNT</label>
                                            <input class="form-control" name="gross" type="number" id="paye"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button  id="myButton" class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>SUBMIT</button>&nbsp;

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>



                </div>
            </div>
        </div>


@endsection
