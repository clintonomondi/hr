@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(!empty($month))
                        <h3 id="dataname" > PAYE REPORTS FOR {{date('F', mktime(0, 0, 0, $month, 10))}}     {{$year}}</h3>
                    @else
                        <h3 id="dataname" > ALLOWANCE REPORTS</h3>
                    @endif

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('report/allowance')}}">
                            @csrf
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <span class="align-right">
                                        <select class="form-control" id="sel1" name="benefit_id"  required>
                                             <option disabled="disabled" selected="selected">Select Allowance</option>
                                            @foreach($deductions as $benefit)
                                                <option value="{{$benefit->id}}">{{$benefit->name}}</option>
                                            @endforeach
                                          </select>
                                        &nbsp; </span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select class="form-control" id="report" name="date" onchange="submitform()">
                                        <option value='0'>-- Select month--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select class="form-control" id="reportr" name="year" onchange="submitform()">
                                        <option disabled="disabled" selected="selected">Select year</option>
                                        @foreach($years as $year)
                                            <option >{{$year->year}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>

                        </form>
                    </div>

                    @include('includes.message')
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Allowance Name</th>
                                    <th>Staff Name.</th>
                                    <th>Staff No.</th>
                                    <th>Payslip.</th>
                                    <th>Amount</th>
                                    <th>Month</th>
                                    <th>Year</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datas as  $key=>$data)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$data->benefit->name}} </td>
                                        <td>{{$data->payslip->employee->fname}} {{$data->payslip->employee->lname}}</td>
                                        <td>{{$data->payslip->employee->regno}}</td>
                                        <td>{{$data->payslip->payslip_no}}</td>
                                        <td>{{number_format($data->amount,2)}}</td>
                                        <td>{{date('F', mktime(0, 0, 0, $data->month, 10))}}</td>
                                        <td>{{$data->year}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
