@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(empty($user))
                        <h3 id="dataname" > Deactivations Report</h3>
                    @else
                        <h3 id="dataname" >Deactivations Report For {{$user->fname}}  {{$user->regno}}</h3>
                    @endif
                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('report/getDeactivationByUser')}}">
                            @csrf
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('includes.message')
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Staff Name</th>
                                    <th>Staff No.</th>
                                    <th>Phone</th>
                                    <th>Reason for Deactivation</th>
                                    <th>Comment</th>
                                    <th>Created By</th>
                                    <th>Deactivated At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datas as  $key=>$data)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$data->employee->fname}} {{$data->employee->lname}} </td>
                                        <td>{{$data->employee->regno}}</td>
                                        <td>{{$data->employee->phone}}</td>
                                        <td>{{$data->reason}}</td>
                                        <td>{!! $data->comment !!}</td>
                                        <td>{{$data->user->employee->regno}}</td>
                                        <td>{{$data->created_at}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
