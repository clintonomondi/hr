@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    @if(!empty($month))
                    <h3 id="dataname" > PAYE REPORTS FOR {{date('F', mktime(0, 0, 0, $month, 10))}}     {{$year}}</h3>
                        @else
                        <h3 id="dataname" > PAYE REPORTS</h3>
                    @endif

                </div>
                <div class="box-content">
                    <div class="row justify-content-center">
                        <form method="post" id="reportform" name="reportform" action="{{route('report/postPayeReport')}}">
                            @csrf
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="report" name="date" onchange="submitform()">
                                        <option value='0'>-- Select month--</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="reportr" name="year" onchange="submitform()">
                                        <option disabled="disabled" selected="selected">Select year</option>
                                        @foreach($years as $year)
                                        <option >{{$year->year}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="selUser" name="id" onchange="submitform()">
                                        <option value='0'>-- Select employee --</option>
                                    </select>
                                </div>
                            </div>

                        </form>
                    </div>

                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="{{route('report/paye')}}">PAYE</a></li>
                        <li><a href="{{route('report/nhif')}}">NHIF</a></li>
                        <li><a href="{{route('report/nssf')}}">NSSF</a></li>
                    </ul>

                    @include('includes.message')
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Staff No.</th>
                                    <th>ID NO.</th>
                                    <th>PIN</th>
                                    <th>BASIC SALARY</th>
                                    <th>PAYE AMOUNT</th>
                                    <th>MONTH</th>
                                    <th>YEAR</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datas as  $key=>$data)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$data->payslip->employee->fname}}  {{$data->payslip->employee->lname}}</td>
                                        <td>{{$data->payslip->employee->regno}}</td>
                                        <td>{{$data->payslip->employee->idno}}</td>
                                        <td>{{$data->payslip->employee->pin}}</td>
                                        <td>{{number_format($data->payslip->employee->payroll->gross,2)}}</td>
                                        <td>{{number_format($data->amount,2)}}</td>
                                        <td>{{date('F', mktime(0, 0, 0, $data->month, 10))}}</td>
                                        <td>{{$data->year}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
