@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >PAYSLIP</h3>
                </div>
                <div class="box-content">

                    <div class="clearfix">

                    </div>

                    <div class="col-md-6 invoice-info">
                        <a href="#" class="btn btn-circle btn-primary btn-xxlarge show-tooltip" title="Download PDF" onclick="generatePDF('{{$payslip->payslip_no}}')"><i class="fa fa-print"></i></a>
{{--                        <a class="btn btn-circle show-tooltip" onclick="generatePDF('{{$payslip->payslip_no}}')" title="Print" href="#" id="printButton"><i class="fa fa-print"></i></a>--}}
{{--                        <a class="btn btn-circle show-tooltip" onclick="generatePDF2()" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>--}}
                    </div>

                    <div class="invoice" style="padding: 50px;" id="printElement">
                        <div class="row justify-content-center">
                            <img   src="{{asset('images/logo.png')}}" alt="logo" style="width:180px" />
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="p2 ft4"><span class="ft0">P</span>AYSLIP</p>
                            </div>
                            <div class="col-md-6 invoice-info">
                                <div>
                                    <img id="example">
                                </div>
                            </div>
                        </div>

                        <hr class="margin-0" /><br>
                        <div class="row">
                            <div class="col-sm-6">
                                <a class="btn btn-info btn-sm">EMPLOYEE INFORMATION</a><br>
                                <strong>FULL NAME</strong>
                                <p>{{$payslip->employee->fname}}  {{$payslip->employee->lname}}</p>
                                <strong>PHONE</strong>
                                <p>{{$payslip->employee->phone}}</p>
                                <strong>EMAIL</strong>
                                <p>{{$payslip->employee->email}}</p>
                            </div>
                            <div class="col-sm-6">
                                <table class="table table-bordered">

                                    <tbody>
                                    <tr>
                                        <td class="bg-info">PAY DATE</td>
                                        <td class="bg-info">PAY TYPE</td>
                                        <td class="bg-info">PERIOD</td>
                                    </tr>
                                    <tr>
                                        <td>{{$payslip->date}}</td>
                                        <td>MONTHLY</td>
                                        <td>{{date('F', mktime(0, 0, 0, $payslip->month, 10))}}   {{$payslip->year}}</td>
                                    </tr>
                                    <tr>
                                        <td class="bg-info">PAYSLIP NO.</td>
                                        <td class="bg-info">EMPLOYEE NO.</td>
                                        <td class="bg-info">TAX CODE</td>
                                    </tr>
                                    <tr>
                                        <td id="payslip_no">{{$payslip->payslip_no}}</td>
                                        <td>{{$payslip->employee->regno}}</td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered payslipTable" >

                                        <tr class="bg-info">
                                            <th>EARNINGS</th>
                                            <th>HOURS</th>
                                            <th>RATE</th>
                                            <th>CURRENT</th>
                                        </tr>
                                        <tbody>
                                        @foreach($benefits as $key=>$benefit)
                                            <tr>
                                                <td>{{$benefit->name}}</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>Ksh. {{number_format($benefit->amount,2)}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr class="bg-info">
                                            <th></th>
                                            <th></th>
                                            <th>GROSS PAY</th>
                                            <th>KSH {{number_format($ti,2)}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered payslipTable">

                                        <tr class="bg-info">
                                            <th>DEDUCTIONS</th>
                                            <th></th>
                                            <th>CURRENT</th>
                                        </tr>
                                        <tbody>
                                        @foreach($deductions as $key=>$deduction)
                                            <tr>
                                                <td>{{$deduction->name}}</td>
                                                <td></td>
                                                <td>Ksh. {{number_format($deduction->amount,2)}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr class="bg-primary">
                                            <th>NET EARNING</th>
                                            <th></th>
                                            <th >KSH {{number_format($net,2)}}</th>
                                        </tr>
                                        </tfoot>
                                        <tfoot>
                                        <tr class="bg-info">
                                            <th>TOTAL DEDUCTION</th>
                                            <th></th>
                                            <th> KSH {{number_format($deductions->sum('amount'),2)}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
