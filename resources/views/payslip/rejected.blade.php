@extends('layouts.app')

@section('content')
    <form id="approveForm" name="approveForm" method="post" onsubmit="return false">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-title">
                        {{--                    <h3 id="dataname" >PAYSLIPS FOR {{date('F', mktime(0, 0, 0, $month, 10))}}     {{$year}}</h3>--}}
                        <h3 id="dataname" >REJECTED PAYSLIPS FOR BATCH {{$batch_id}}</h3>
                    </div>
                    <div class="box-content">

                        <div class="clearfix">

                            <div class="pull-right">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <a href="{{route('payslip/index')}}" class="btn btn-info show-tooltip" title="Back to Summery "><i class="fa fa-backward"></i>BACK TO SUMMERY</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('includes.message')
                        <hr>
                        <input hidden type="text" name="batch_id" value="{{$batch_id}}">
                        <table class="table table-bordered table-hover table-striped" id="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>NAME</th>
                                <th>Staff Number</th>
                                <th>Payslip No.</th>
                                <th>Remarks.</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payslips as  $key=>$payslip)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$payslip['name']}}</td>
                                    <td>{{$payslip['regno']}}</td>
                                    <td>{{$payslip['payslip_no']}}</td>
                                    <td>{!! $payslip['remarks'] !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                    </div>

                </div>
            </div>

        </div>
    </form>
@endsection
