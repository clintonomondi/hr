@extends('layouts.app')

@section('content')
<form id="approveForm" name="approveForm" method="post" onsubmit="return false">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
{{--                    <h3 id="dataname" >PAYSLIPS FOR {{date('F', mktime(0, 0, 0, $month, 10))}}     {{$year}}</h3>--}}
                    <h3 id="dataname" >PAYSLIPS FOR BATCH {{$batch_id}}</h3>
                </div>
                <div class="box-content">
                    <p class="text-danger text-center"><strong></strong>**Please note that: Other Deductions and Allowances are calculated in the payslip after approval**</p>
                    <div class="clearfix">
                        <div class="pull-left ml-5">
                            <a href="{{route('payslip/print',$batch_id)}}" class="btn btn-primary btn-sm fa fa-print">Print</a>
                        </div>

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="#"  data-toggle="modal" data-target="#approve"  class="btn btn-success show-tooltip" title="Clinck to approve "><i class="fa fa-check"></i>APPROVE PAYSLIPS</a>
                                        </div>
                                        <div class="col-sm-4">
                                            <a href="#" data-toggle="modal" data-target="#reject" class="btn btn-warning show-tooltip" title="Clinck reject "><i class="fa fa-cross"></i>REJECT PAYSLIPS</a>
                                        </div>

                                        <div class="col-sm-4">
                                    <a href="{{route('payslip/index')}}" class="btn btn-info show-tooltip" title="Back to Summery "><i class="fa fa-backward"></i>BACK TO SUMMERY</a>
                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    @include('includes.message')
                    @include('modals.approvepayslip')
                    @include('modals.rejectpayslip')
                    <hr>
                    <input hidden type="text" name="batch_id" value="{{$batch_id}}">
                    <table class="table table-bordered table-hover table-striped" id="table8">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>NAME</th>
                            <th>Staff Number</th>
                            <th>Payslip No.</th>
                            <th>Basic Salary</th>
                            <th>PAYE</th>
                            <th>NHIF</th>
                            <th>NSSF</th>
                            <th>Allowances</th>
                            <th>Deductions</th>
                            <th>Personal Relief</th>
                            <th>Net Income</th>
                            <th></th>
                            <th class="text-success"><input type="checkbox" value="" id="selectAll" /> SELECT ALL</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payslips as  $key=>$payslip)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$payslip['name']}}</td>
                                <td>{{$payslip['regno']}}</td>
                                <td>{{$payslip['payslip_no']}}</td>
                                <td>{{number_format($payslip['basic'],2)}}</td>
                                <td>{{number_format($payslip['paye'],2)}}</td>
                                <td>{{number_format($payslip['nhif'],2)}}</td>
                                <td>{{number_format($payslip['nssf'],2)}}</td>
                                <td>{{number_format($payslip['benefit'],2)}}</td>
                                <td>{{number_format($payslip['deductions'],2)}}</td>
                                <td>{{number_format($payslip['tax_relief'],2)}}</td>
                                <td>{{number_format($payslip['net'],2)}}</td>
                                <td> <a class="btn btn-info btn-sm" href="{{route('payslip/view',$payslip['id'])}}"><i class="fa fa-eye"></i>View Payslip</a></td>

                                <td><input type="checkbox" value="{{$payslip['payslip_no']}}" name="payslip_no[]" checked /></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>



                </div>

            </div>
        </div>

    </div>
</form>
@endsection
