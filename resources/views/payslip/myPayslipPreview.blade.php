@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >PAYSLIP PREVIEW</h3>

                </div>
                <div class="box-content">
                @include('includes.message')
                <!-- BEGIN Tiles -->
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="tile">
                                <p class="title">TRIGALS COMPANY</p>
                                    <p>You have a Basic Salary of Ksh <strong>Ksh. {{number_format($basic,2)}}</strong>.</p>
                                    <p>You are entitled to a personal relief of Ksh <strong>Ksh. {{number_format($personal->sum('amount'),2)}}</strong>.</p>
                                    <p>Taxable Income Ksh <strong>Ksh. {{number_format($TI,2)}}</strong>.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="tile">
                                    <p class="title">TRIGALS COMPANY</p>
                                    <p>You are paying PAYE less Personal relief  Ksh. {{number_format($paye,2)}}</p>
                                    <p>Other Benefits  Ksh. {{number_format($benefits->sum('amount'),2)}}</p>
                                    <p>Other Deductions Total Balance Ksh. {{number_format($deductions->sum('balance'),2)}}</p>


                                    <div class="img img-bottom">
                                        <i class="fa fa-money"></i>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="tile">
                                <p class="title">TRIGALS COMPANY</p>
                                <p>NHIF <strong>Ksh. {{number_format($nhif,2)}}</strong>.</p>
                                <p>NSSF <strong>Ksh. {{number_format($nssf,2)}}</strong>.</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tile">
                                <p class="title">CURRENT NET PAY</p>
                                <h2>KSH {{number_format($net,2)}}</h2>
                                <p>Other Deductions to Pay this month  Ksh. {{number_format($deductions->sum('amount_deducted'),2)}}</p>
                                <div class="img img-bottom">
                                    <i class="fa fa-money"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            <p>OTHER ALLOWANCES</p>

                            <table class="table table-bordered table-hover table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee Name</th>
                                    <th>Staff No.</th>
                                    <th>Allowance Name</th>
                                    <th>Amount</th>
                                    <th>Date created</th>
                                    <th>Added by</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($benefits as $key=>$benefit)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$benefit->employee->fname}} {{$benefit->employee->lname}}</td>
                                        <td>{{$benefit->employee->regno}}</td>
                                        <td>{{$benefit->allowance->name}}</td>
                                        <td>{{number_format($benefit->amount,2)}}</td>
                                        <td>{{$benefit->date}}</td>
                                        <td>{{$benefit->user->name}}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <p>OTHER DEDUCTIONS</p>

                            <table class="table table-bordered table-hover table-striped" id="table2">
                                <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Deduction name</th>
                                <th>Total Amount</th>
                                <th>Frequency(Months)</th>
                                <th>Monthly Deduction</th>
                                <th>Balance</th>
                                <th>Expiry</th>
                                </thead>
                                <tbody>
                                @foreach($deductions as $key=>$deduction)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$deduction->employee->fname}} {{$deduction->employee->lname}}</td>
                                        <td>{{$deduction->deduction->name}}</td>
                                        <td>{{number_format($deduction->amount,2)}}</td>
                                        <td>{{$deduction->frequency}}  Months</td>
                                        <td>{{number_format($deduction->amount_deducted,2)}}</td>
                                        <td>{{number_format($deduction->balance,2)}}</td>
                                        <td>{{$deduction->expiry}}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
