@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >PAYSLIPS MONTHLY SUMMERY</h3>
                </div>
                <div class="box-content">

                    <div class="clearfix">
<p class="text-danger text-center"><strong></strong>**Please note that: Other Deductions and Allowances are calculated in the payslip after approval**</p>
                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <a href="{{route('payslip/generate')}}" class="btn btn-primary show-tooltip" title="Upload new images"><i class="fa fa-check"></i> GENERATE PAYSLIPS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table class="table table-bordered table-hover table-striped" id="table">
                        @include('includes.message')
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>BATCH</th>
                            <th>MONTH</th>
                            <th>YEAR</th>
                            <th>TOTAL AMOUNT</th>
                            <th>DATE</th>
                            <th>TOTAL PAYSLIPS</th>
                            <th>APPROVED</th>
                            <th>REJECTED</th>
                            <th>STATUS</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payslips as  $key=>$user)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$user->batch_id}}</td>
                                <td>{{date('F', mktime(0, 0, 0, $user->month, 10))}}</td>
                                <td>{{$user->year}}</td>
                                <td>{{number_format($user->netamount-$user->deductionamount,2)}}</td>
                                <td>{{$user->date}}</td>
                                <td>{{$user->total}}</td>
                                <td>{{$user->totalapproved}}</td>
                                <td class="text-danger"><a href="{{route('payslip/rejected',$user->batch_id)}}">{{$user->rejected}}</a></td>
                                @if($user->status=='unapproved')
                                <td class="text-danger" >{{$user->status}}</td>
                                @else
                                    <td class="text-success">{{$user->status}}</td>
                                    @endif
                                <td>
                                    <a class="btn btn-info btn-sm" href="{{route('payslip/payslips',$user->batch_id)}}"><i class="fa fa-eye"></i>View Payslips</a>
                                    @if($user->status=='approved')
                                    <a class="btn btn-warning btn-sm" href="#" onclick="Dispatch('{{$user->batch_id}}')"><i class="fa fa-envelope"></i>Dispatch</a>
                                    @endif
{{--                                    <a class="btn btn-success btn-sm" href="{{route('payslip/payslips',$user->date)}}"><i class="fa fa-check"></i>Approve Payslips</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>



                </div>

            </div>
        </div>

    </div>

@endsection
