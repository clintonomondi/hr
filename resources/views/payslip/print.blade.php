@extends('layouts.app')

@section('content')
    <form id="approveForm" name="approveForm" method="post" onsubmit="return false">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-content">
                        <div class="clearfix">
                            <div class="pull-left ml-5">
                                <a href="#" onclick="generatePDF('{{date('F', mktime(0, 0, 0, $date->month, 10))}} {{$date->year}}')" class="btn btn-primary btn-sm fa fa-print">Print</a>
                            </div>

                            <div class="pull-right">
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <a href="{{route('payslip/index')}}" class="btn btn-info show-tooltip" title="Back to Summery "><i class="fa fa-backward"></i>BACK TO SUMMERY</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="invoice7" style="padding: 50px;" id="printElement">
                        <div class="row justify-content-center">
                            <img   src="{{asset('images/logo.png')}}" alt="logo" style="width:180px" />
                        </div>
                            <h5 class="text-center">PAYSLIPS FOR {{date('F', mktime(0, 0, 0, $date->month, 10))}} {{$date->year}}</h5>

                        <table class="table table-bordered table-hover table-striped" id="table8">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>NAME</th>
                                <th>Staff No.</th>
                                <th>Basic Salary</th>
                                <th>PAYE</th>
                                <th>NHIF</th>
                                <th>NSSF</th>
                                <th>Allowances</th>
                                <th>Deductions</th>
                                <th>Net Income</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payslips as  $key=>$payslip)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$payslip['name']}}</td>
                                    <td>{{$payslip['regno']}}</td>
                                    <td>{{number_format($payslip['basic'],2)}}</td>
                                    <td>{{number_format($payslip['paye'],2)}}</td>
                                    <td>{{number_format($payslip['nhif'],2)}}</td>
                                    <td>{{number_format($payslip['nssf'],2)}}</td>
                                    <td>{{number_format($payslip['benefit'],2)}}</td>
                                    <td>{{number_format($payslip['deductions'],2)}}</td>
                                    <td>{{number_format($payslip['net'],2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                            <hr>

                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <p>Prepared By ..................................................</p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Date..................................................</p>
                                </div>

                            </div>
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <p>Approved  By ..................................................</p>
                                </div>
                                <div class="col-sm-6">
                                    <p>Date..................................................</p>
                                </div>

                            </div>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </form>
@endsection
