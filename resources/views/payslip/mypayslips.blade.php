@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >MY PAYSLIPS {{Auth::user()->name}}</h3>
                </div>
                <div class="box-content">

                    <div class="clearfix">

                        <div class="pull-right">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table class="table table-bordered table-hover table-striped" id="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Staff Number</th>
                            <th>Payslip No.</th>
                            <th>Basic Salary</th>
                            <th>PAYE</th>
                            <th>NHIF</th>
                            <th>NSSF</th>
                            <th>Allowances</th>
                            <th>Deductions</th>
                            <th>Personal Relief</th>
                            <th>Net Income</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($payslips))
                        @foreach($payslips as  $key=>$payslip)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{date('F', mktime(0, 0, 0, $payslip['month'], 10))}}   {{$payslip['year']}}</td>
                                <td>{{$payslip['regno']}}</td>
                                <td>{{$payslip['payslip_no']}}</td>
                                <td>{{number_format($payslip['basic'],2)}}</td>
                                <td>{{number_format($payslip['paye'],2)}}</td>
                                <td>{{number_format($payslip['nhif'],2)}}</td>
                                <td>{{number_format($payslip['nssf'],2)}}</td>
                                <td>{{number_format($payslip['benefit'],2)}}</td>
                                <td>{{number_format($payslip['deductions'],2)}}</td>
                                <td>{{number_format($payslip['tax_relief'],2)}}</td>
                                <td>{{number_format($payslip['net'],2)}}</td>
                                <td> <a class="btn btn-info btn-sm" href="{{route('payslip/view',$payslip['id'])}}"><i class="fa fa-eye"></i>View Payslip</a></td>
                            </tr>
                        @endforeach
                            @endif
                        </tbody>
                    </table>



                </div>

            </div>
        </div>

    </div>

@endsection
