@extends('layouts.app')

@section('content')
    <form method="post"  action="{{route('payslip/generate')}}">
        @csrf
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname">PAYSLIPS</h3>
                </div>
                <div class="box-content">
                    <p class="text-danger text-center"><strong></strong>**Please note that: Other Deductions and Allowances are calculated in the payslip after approval**</p>
                    <div class="row justify-content-center">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">SELECT DATE</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
{{--                                <input class="form-control" name="date" type="date" placeholder="Enter date" required>--}}


                                        <div class="input-group date date-picker" data-date="102/2012" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control date-picker" name="date" size="16" type="text" required>
                                        </div>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary show-tooltip" title="Generate Payslips"><i class="fa fa-check"></i> GENERATE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>

<div class="row justify-content-center">
                 <div class="col-sm-12">
                     @include('includes.message')
                     <table class="table table-bordered table-hover table-striped" >
                         <thead>
                         <tr>
                             <th>#</th>
                             <th>Name</th>
                             <th>Staff No.</th>
                             <th>Title</th>
                             <th>Basic Salary</th>
                             <th class="text-success"><input type="checkbox" value="" id="selectAll" /> SELECT ALL</th>
                         </tr>
                         </thead>
                         <tbody>
                         @foreach($users as  $key=>$user)
                             <tr>
                                 <td>{{$key+1}}</td>
                                 <td>{{$user->fname}}  {{$user->lname}}</td>
                                 <td>{{$user->regno}}</td>
                                 <td>{{$user->title}}</td>
                                 @if(empty($user->payroll->gross))
                                 <td>0.00</td>
                                 @else
                                 <td>{{number_format($user->payroll->gross,2)}}</td>
                                     @endif
                                 <td><input type="checkbox" value="{{$user->id}}" name="employee_id[]"  /></td>
                             </tr>
                         @endforeach
                         </tbody>
                     </table>


                 </div>
</div>

                </div>

            </div>
    </form>

@endsection
