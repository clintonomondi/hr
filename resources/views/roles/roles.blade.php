@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" > Roles</h3>

                </div>
                <div class="box-content">
                    @include('includes.message')
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="{{route('roles')}}">Active Roles</a></li>
                        <li><a href="{{route('roles.inactive')}}">InActive Roles</a></li>
                    </ul>
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <table class="table table-bordered table-hover table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Role.</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Change Role</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as  $key=>$user)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->role}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>
                                            <select class="form-control" id="sel1" name="role" onchange="updateRole(this.value,'{{$user->id}}')">
                                                <option>{{$user->role}}</option>
                                                <option class="text-success">admin</option>
                                                <option class="text-success">supervisor</option>
                                                <option class="text-danger">user</option>
                                            </select>
                                        </td>

                                        @if($user->status=='Active')
                                        <td class="text-success">{{$user->status}}</td>
                                        @else
                                        <td class="text-danger">{{$user->status}}</td>
                                        @endif
                                        <td>
                                            @if($user->status=='Active')
                                                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal{{$user->id}}" href="#"><i class="fa fa-eye"></i> Deactivate</a>
                                            @else
                                            <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal2{{$user->id}}" href="#" ><i class="fa fa-eye"></i> Activate</a>
                                                @endif
                                        </td>
                                    </tr>
                                    @include('modals.deactivate')
                                    @include('modals.activate')
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
