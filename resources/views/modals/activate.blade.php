<div id="modal2{{$user->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Confirmation</h3>
            </div>
                @csrf
                <div class="modal-body">
                    <p>Are you sure you want to <span class="text-success">Activate</span> <b>{{$user->name}}</b>??</p>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" onclick="updateStatus('Active','{{$user->id}}')" class="btn btn-success" >Activate</button>
                </div>
        </div>
    </div>
</div>
