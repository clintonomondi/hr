<div id="verify" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2"></h3>
            </div>
            <div class="modal-body">

<div class="upperdiv" id="upperdiv">
                <p class="text-center" id="name"></p>
                <p class="text-center" id="email"></p>
                <p class="text-center" id="phone2"></p>
                <p class="text-center text-info">**Please Enter six digit verification number**</p>
                <div class="container">

                        <div class="row px-3"> <label class="mb-1">
                        <h6 class="mb-0 text-sm">Enter Code</h6>
                        </label> <input class="mb-4" id="code" type="text" name="code" placeholder="Enter six digit code">
                        </div>
                        <div class="row mb-3 px-3"> <button id="verifybtn" onclick="verifyCode()"   class="btn btn-blue text-center">Verify Code</button> </div>
                </div>

</div>

                <div class="lowerdiv" id="lowerdiv">
                    <p class="text-center" id="name"></p>
                    <p class="text-center text-info">**Please Enter new password**</p>
                    <div class="container">

                        <div class="row px-3"> <label class="mb-1">
                                <h6 class="mb-0 text-sm">Password</h6>
                            </label> <input class="mb-4" id="password" type="password" name="password" placeholder="Enter password">
                        </div>
                        <div class="row px-3"> <label class="mb-1">
                                <h6 class="mb-0 text-sm">Confirm password</h6>
                            </label> <input class="mb-4" id="repass" type="password" name="repass" placeholder="Confirm password">
                        </div>
                        <div class="row mb-3 px-3"> <button id="setpasswordbtn" onclick="setPassowrd()"   class="btn btn-blue text-center">SUBMIT</button> </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
