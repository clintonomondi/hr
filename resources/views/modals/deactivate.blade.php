<div id="modal{{$user->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Confirmation</h3>
            </div>
            <form method="post" action="{{route('updateStatus2')}}">
                @csrf
            <div class="modal-body">
                <p>Are you sure you want to <span class="text-danger">Deactivate</span> <b>{{$user->name}}</b>??</p>
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select class="form-control" id="sel1" name="reason" required>
                                <option disabled="disabled" selected="selected">Select reason for deactivation</option>
                                <option >Resignation</option>
                                <option >Dismissal</option>
                                <option >Termination</option>
                                <option >Expiry of Contract</option>
                                <option >Desertion</option>
                                <option >Retirement</option>
                                <option >Death</option>
                                <option >Medical Grounds </option>
                            </select>
                        </div>
                    </div>
                </div>
<input type="text" name="id" value="{{$user->id}}" hidden>
<input type="text" name="status" value="Inactive" hidden>
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Comment for Deactivation (Optional)</label>
                            <textarea class="form-control" rows="3" name="comment" ></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button type="submit" class="btn btn-danger" >Deactivate</button>
            </div>
            </form>
        </div>
    </div>
</div>
