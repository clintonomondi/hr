<!-- Modal -->
<div class="modal fade" id="e{{$kpi->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{route('performance/record')}}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Record {{$kpi->kpi->kpi}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <p>Record Performance for KPI: {{$kpi->kpi->kpi}}</p>
                    <p>{{$kpi->employee->fname}} {{$kpi->employee->lname}}</p>
                    <p>Target: {{$kpi->target}}</p>
                    <div class="form-group">
                        <label class="control-label">Enter Unit</label>
                        <input class="form-control" name="unit" type="number"  required>
                        <input  name="performance_id" value="{{$kpi->id}}" type="text"  required hidden>
                        <input  name="user_id" value="{{$kpi->employee_id}}" type="text"  required hidden>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                    <button type="submit" class="btn btn-primary" >SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>
