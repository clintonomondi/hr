<div id="re{{$leave->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{route('leave/recall',$leave->id)}}">
                @csrf
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Confirmation</h3>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to <b>RECALL</b> this leave??</p>
                <p>Remaining days <b>{{number_format((strtotime($leave->end_date)-strtotime(now()))/(60 * 60 * 24))}} days</b></p>

                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Reason for Recall (Optional)</label>
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>
                    </div>
                </div>
                <input type="text" name="remainder" value="{{number_format((strtotime($leave->end_date)-strtotime(now()))/(60 * 60 * 24))}}" hidden>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button type="submit" class="btn btn-warning">recall</button>
            </div>
            </form>
        </div>
    </div>
</div>
