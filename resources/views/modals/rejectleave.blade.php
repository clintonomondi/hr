<div id="{{$leave->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Confirmation</h3>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to reject this request??</p>

                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Reason for rejection (Optional)</label>
                            <textarea class="form-control" rows="3" name="comment" id="comment{{$leave->id}}"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-primary" data-dismiss="modal" onclick="changeLeave('{{$leave->id}}','rejected',document.getElementById('comment{{$leave->id}}').value)">Reject</button>
            </div>
        </div>
    </div>
</div>
