<!-- Modal -->
<div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h6>PAYSLIP FOR {{$batch_id}}</h6>
                        <p>Are you sure you want to approve this payslips for  <strong>{{$batch_id}}</strong></p>
<p>Please Note: Only payslips selected will be approved, the rest will be automatically rejected</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                <button type="submit" onclick="approvePayslips()"  class="btn btn-success" >APPROVE</button>
            </div>
        </div>
    </div>
</div>
