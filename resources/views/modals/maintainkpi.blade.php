<!-- Modal -->
<div class="modal fade" id="allowance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{route('kpa/push')}}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Maintain KPI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                                    <div class="form-group">
                                        <label class="control-label">KPI</label>
                                        <input class="form-control" name="kpi" type="text"  required>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Unit of Measure</label>
                                        <input class="form-control" name="unit" type="text"  required>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Target</label>
                                        <input class="form-control" name="target" type="text"  required>
                                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                    <button type="submit" class="btn btn-primary" >SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>
