<!-- Modal -->
<div class="modal fade" id="b{{$deduction->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h6>KSH. {{number_format($deduction->amount,2)}}           {{$deduction->name}}</h6>
                        <p>Are you sure you want to remove this deduction from  <strong>{{$deduction->employee->fname}}</strong></p>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                <a href="{{route('deduction/remove',$deduction->id)}}" class="btn btn-danger" >REMOVE</a>
            </div>
        </div>
    </div>
</div>
