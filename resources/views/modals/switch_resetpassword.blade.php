<div id="switch" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2"></h3>
            </div>
            <div class="modal-body">
                <p class="text-center text-info">Please Choose how you want to reset password</p>
                <div class="container">
                <div class="row justify-content-center">

                    <div class="col-sm-6">
                        <a href="{{route('password.request')}}" class="btn switchbtn"><i class="fa fa-envelope"></i> EMAIL</a>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{route('password/send/otp')}}" class="btn switchbtn"><i class="fa fa-mobile-phone"></i> OTP SMS</a>
                    </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
