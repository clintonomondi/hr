<!-- Modal -->
<div class="modal fade" id="e{{$deduction->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{route('deduction/update',$deduction->id)}}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit {{$deduction->employee->fname}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input class="form-control" name="name" type="text" value="{{$deduction->name}}"   required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Amount to (Ksh)</label>
                                <input class="form-control" name="amount" type="number" id="amount{{$deduction->id}}" value="{{$deduction->amount}}" onkeyup="calculateMonthly2({{$deduction->id}})"   required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Frequency(Months)</label>
                                <input class="form-control" name="frequency" type="number"  id="frequency{{$deduction->id}}" value="{{$deduction->frequency}}"  onkeyup="calculateMonthly2({{$deduction->id}})"   required>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Balance</label>
                                <input disabled class="form-control" name="balance" type="number" id="balance{{$deduction->id}}" value="{{$deduction->balance}}"    style="color: blue"   required>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Monthly Deduction</label>
                                <input disabled class="form-control" name="amount_deducted" type="number" id="amount_deducted{{$deduction->id}}" value="{{$deduction->amount_deducted}}"    style="color: blue"   required>
                                <hr><span style="color: green;"  id="monthly{{$deduction->id}}"></span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                    <button type="submit" class="btn btn-primary" >SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>
