<!-- Modal -->
<div class="modal fade" id="e{{$basic->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{route('basic/update',$basic->id)}}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit {{$basic->employee->fname}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row justify-content-center">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">{{$basic->employee->fname}}  Ksh. {{$basic->gross}}</label>
                                    </div>
                                </div>
                            </div><hr>
                            <div class="row justify-content-center">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">BASIC SALARY AMOUNT</label>
                                        <input class="form-control" name="gross" type="number" value="{{$basic->gross}}"  required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                    <button type="submit" class="btn btn-primary" >SUBMIT</button>
                </div>
            </div>
        </form>
    </div>
</div>
