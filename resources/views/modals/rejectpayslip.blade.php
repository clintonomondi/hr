<!-- Modal -->
<div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h6>PAYSLIP FOR {{$batch_id}}</h6>
                        <p>Are you sure you want to REJECT these payslips for  <strong>{{$batch_id}}</strong></p>

                    </div>

                </div>
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Reason for rejection (Optional)</label>
                            <textarea class="form-control" rows="3" name="remarks" id="comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">CANCEL</button>
                <button onclick="rejectPayslips()" class="btn btn-success" >REJECT</button>
            </div>
        </div>
    </div>
</div>
