<div id="edit{{$paye->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Edit PAYE</h3>
            </div>
            <form method="post" name="myForm"  action="{{route('updatePaye',$paye->id)}}">
                @csrf
                <div class="modal-body">

                        @csrf
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Amount from (Ksh)</label>
                                    <input class="form-control" name="from_amount" type="number" value="{{$paye->from_amount}}"   required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Amount to (Ksh)</label>
                                    <input class="form-control" name="to_amount" type="number" value="{{$paye->to_amount}}"   required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Percentage (%)</label>
                                    <input class="form-control" name="percentage" type="number" value="{{$paye->percentage}}"   required>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
