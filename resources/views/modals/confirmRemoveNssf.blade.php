<div id="remove{{$paye->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel2">Confirmation</h3>
            </div>

            <div class="modal-body">
                <p>Are you sure you want to <span class="text-danger">Remove</span> <b>This range</b>??</p>


            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <a  href="{{route('removeNssfItem',$paye->id)}}" class="btn btn-danger" >Remove</a>
            </div>
        </div>
    </div>
</div>
