@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >PERSONAL RELIEF</h3>

                </div>
                <div class="box-content">
@include('includes.message')
                    <!-- BEGIN Tiles -->
                    <div class="row justify-content-center">
                                        <div class="col-md-6">
                                            <div class="tile">
                                                <p class="title">TRIGALS COMPANY</p>
                                               @if(empty($personal))
                                                <p>Every employee is entitled to a personal relief of  <strong> KSh. 0.00</strong>.</p>
                                                @else
                                                <p>Every employee is entitled to a personal relief of Ksh <strong>Ksh. {{number_format($personal->sum('amount'),2)}}</strong>.</p>
                                                @endif
                                                <div class="img img-bottom">
                                                    <i class="fa fa-money"></i>
                                                </div>
                                            </div>
                                        </div>
                        @if(empty($personal))
                            <div class="col-md-6">
                                <div class="tile">
                                    <p class="title">CREATE PERSONAL</p>
                                        <p>Every employee is entitled to this personal relief.</p>
                                        <form method="post" action="{{route('personal/post')}}">
                                            @csrf
                                            <div class="row justify-content-center">
                                                <div class="col-sm-4">
                                                    <input class="form-control" name="name" type="text" value="Personal Relief" disabled   required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input class="form-control" name="amount" type="number" placeholder="Enter amount"   required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button type="submit" class="btn btn-primary" >CREATE</button>
                                                </div>
                                            </div>
                                        </form>
                                    <div class="img img-bottom">
                                        <i class="fa fa-money"></i>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-md-6">
                                <div class="tile">
                                    <p class="title">UPDATE PERSONAL</p>
                                    <p>Every employee is entitled to this personal relief.</p>
                                    <form method="post" action="{{route('personal/update',$personal->id)}}">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <div class="col-sm-4">
                                                <input class="form-control" name="name" type="text" value="{{$personal->name}}" disabled   required>
                                            </div>
                                            <div class="col-sm-4">
                                                <input class="form-control" name="amount" type="number" value="{{$personal->amount}}" placeholder="Enter amount"   required>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary" >UPDATE</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="img img-bottom">
                                        <i class="fa fa-money"></i>
                                    </div>
                                </div>
                            </div>
                            @endif
                                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
