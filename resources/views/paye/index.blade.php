@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3 id="dataname" >GENERAL PAYE DEDUCTIONS</h3>
                </div>
                <div class="box-content">
                    <span class="align-right"> <button class="btn btn-info" onclick="addPayerrange()" type="submit"><i class="fa fa-fw fa-lg fa-plus"></i>Add range</button>&nbsp; </span><hr>
{{--                    @include('includes.message')--}}
                    <div class="row">
                        @include('includes.message')
                        <div class="col-sm-7">
                    <form method="post" name="myForm"  action="{{route('addPaye')}}">
                        @csrf
                        <div id="payerform">


                        </div><hr>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>SAVE All</button>&nbsp;
                    </form>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-sm-12">
                           <table class="table table-striped table-hover table-bordered" id="table">
                               <thead>
                               <th>#</th>
                               <th>Amount from</th>
                               <th>Amount to</th>
                               <th>Range</th>
                               <th>Monthly</th>
                               <th>Annually</th>
                               <th></th>
                               <th></th>
                               </thead>
                               <tbody>
                               @foreach($payes as $key=>$paye)
                                   <tr>
                                       <td>{{$key+1}}</td>
                                       <td>{{number_format($paye->from_amount,2)}}</td>
                                       <td>{{number_format($paye->to_amount,2)}}</td>
                                       <td>{{$paye->percentage}}%</td>
                                       <td>{{number_format($paye->to_amount-$paye->from_amount,2)}}</td>
                                       <td>{{number_format(($paye->to_amount-$paye->from_amount)*12,2)}}</td>
                                       <td> <a data-toggle="modal" data-target="#edit{{$paye->id}}"  class="btn btn-info btn-sm"><i class="fa fa-edit">Edit</i></a></td>
                                       <td> <a data-toggle="modal" data-target="#remove{{$paye->id}}"  class="btn btn-danger btn-sm"><i class="fa fa-ban">Remove</i></a></td>
                                   </tr>
                                   @include('modals.editpaye')
                                   @include('modals.confirmRemovePaye')

@endforeach
                               </tbody>
                           </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
