<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    protected  $fillable=['payslip_no','month','year','date','employee_id','status','approved_by','created_by','batch_id','remarks'];

    public  function payslipdata(){
        return $this->hasMany(Payslip_data::class);
    }

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }

}
