<?php

namespace App\Http\Controllers;

use App\Otp_Logs;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Knox\AFT\AFT;

class OTPController extends Controller
{

    public  function sendotp(){
        return view('auth.passwords.phone');
    }

    public  function generateotp(Request $request){
        $phone=User::where('phone',$request->phone)->first();
        if(empty($phone)){
            return ['status'=>false,'message'=>'The phone number is  not in our records'];
        }
        $code=mt_rand(100000, 999999);
        $request['code']=$code;
        $request['user_id']=$phone->id;
        $request['user_id']=$phone->id;
        Otp_Logs::create($request->all());
       //AFT::sendMessage($request->phone, 'Your six digit password verification code is: '.$code.'  The code will expire after 60 minutes. @BareBalqesaAdvocates');
       $message='Your six digit password verification code is: '.$code.'  The code will expire after 60 minutes.';
       ExternalApiController::sendSMS($request->phone,$message);

        return ['status'=>true,'message'=>'A six digit verification code has been sent,enter the code','info'=>$phone];

    }

    public  function verify(Request $request){
        $otp=Otp_Logs::where('phone',$request->phone)->where('status','unused')->where('code',$request->code)->where('created_at', '>=', Carbon::now()->subMinutes(60)->toDateTimeString())->first();

        if (empty($otp)){
            return ['status'=>false,'message'=>'Invalid code, please try again'];
        }

        return ['status'=>true,'message'=>'Please enter new password','code'=>$request->code];
    }

    public  function set(Request $request){
        if (empty($request->password)){
            return ['status'=>false,'message'=>'Enter phone number'];
        }

        $otp=Otp_Logs::where('phone',$request->phone)->where('status','unused')->where('code',$request->code)->where('created_at', '>=', Carbon::now()->subMinutes(60)->toDateTimeString())->first();

        if (empty($otp)){
            return ['status'=>false,'message'=>'Invalid code, please try again'];
        }

       if ($request->input('password') !== $request->input('repass')) {
           return ['status'=>false,'message'=>'Password are not matching'];
        }
        $id =User::where('phone',$request->phone)->where('email',$request->email)->first();
        $currentUser = User::findOrFail($id->id);
        $currentUser->password = Hash::make($request->input('password'));
        $currentUser->save();

        Otp_Logs::where('phone', $request->phone)->update(['status' => 'used']);

        $message='Your password for Trigals System has been successfully changed.If this is not you, please contact admin';
        ExternalApiController::sendSMS($request->phone,$message);
        return ['status'=>true,'message'=>'Password set successfully'];

    }
}
