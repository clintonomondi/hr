<?php

namespace App\Http\Controllers;

use App\Deactivations;
use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function index(){
        $users=User::where('status','Active')->get();
        return view('roles.roles',compact('users'));
    }
    public  function inactive(){
        $users=User::where('status','Inactive')->get();
        return view('roles.inactive',compact('users'));
    }

    public  function updateRole(Request $request){
        if(Auth::user()->id==$request->id){
            return ['status'=>false,'message'=>'You are not authorised to update this role'];
        }
        $user=User::find($request->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Role updated successfully'];
    }

    public  function updateStatus(Request $request){
        if(Auth::user()->id==$request->id){
            return ['status'=>false,'message'=>'You are not authorised to update this status'];
        }
        $user=User::find($request->id);

        $user->update($request->all());
        $employee=Employee::find($user->employee_id);
       $employee->update($request->all());
        return ['status'=>true,'message'=>'Status updated successfully'];
    }

    public  function updateStatus2(Request $request){
        if(Auth::user()->id==$request->id){
            return redirect()->back()->with('error','You are not authorised to update this user');
        }
        $user=User::find($request->id);

            $request['employee_id']=$user->employee_id;
            $request['user_id']=Auth::user()->id;
            $d=Deactivations::create($request->all());

        $user->update($request->all());
        $employee=Employee::find($user->employee_id);
        $employee->update($request->all());
        return redirect()->back()->with('success','Status updated successfully');
    }

    public  function deactivations(){
        $datas=Deactivations::all();
        return view('reports.deactivations',compact('datas'));
    }

    public  function getDeactivationByUser(Request $request){
        $user=Employee::find($request->id);
        $datas=Deactivations::where('employee_id',$request->id)->get();
        return view('reports.deactivations',compact('datas','user'));
    }
}
