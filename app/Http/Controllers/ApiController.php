<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Deduction;
use App\Employee;
use App\Leave;
use App\MaintainedLeave;
use App\Payroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function apendusers(Request $request){
        $q = $request->search;
        if($q == ''){
            $datas = Employee::orderby('fname','asc')->select('fname','id')->limit(10)->get();;
        }else{
            $datas = Employee::where('fname', 'LIKE', '%' . $q . '%')->orWhere('lname', 'LIKE', '%' . $q . '%')->orWhere('phone', 'LIKE', '%' . $q . '%')->orWhere('regno', 'LIKE', '%' . $q . '%')->get();
        }

        $response = array();
        foreach($datas as $data){
            $response[] = array(
                "id"=>$data->id,
                "text"=>$data->fname.' '.$data->lname
            );
        }
        return $response;
    }

    function getPayslipDate(Request $request){
        $q = $request->search;
        if($q == ''){
            $datas = DB::select( DB::raw("SELECT date as date, MONTHNAME(DATE) AS month,year(date) as year from `payslip_datas` group by date limit 10") );
        }else{
            $datas = DB::select( DB::raw(" SELECT date AS date, MONTHNAME(DATE) AS month,year(date) AS year FROM `payslip_datas` WHERE DATE LIKE '%$q%' OR YEAR LIKE '%$q%' OR  MONTHNAME(DATE) LIKE '%$q%' GROUP BY DATE") );
        }
        $response = array();
        foreach($datas as $data){
            $response[] = array(
                "id"=>$data->date,
                "text"=>$data->month.' '.$data->year,
            );
        }
        return $response;

    }

    public  function postSearch(Request $request){
        $user=Employee::findorfail($request->id);
        return view('employee.view',compact('user'));
    }

    public function postGalarySearch(Request $request){
        $users=Employee::where('id',$request->id)->paginate(8);
        return view('employee.gallery',compact('users'));
    }

    public function getIndividualPayrolldata(Request $request){
        $gross=Payroll::where('employee_id',$request->id)->first();
        $deductions=Deduction::where('employee_id',$request->id)->get();

        return ['gross'=>$gross,'deductions'=>$deductions];
    }

    public function getIndividualPayrolldata2(Request $request){
        $gross=Payroll::where('employee_id',$request->id)->first();
        $benefit=Benefit::where('employee_id',$request->id)->get();

        return ['gross'=>$gross,'benefit'=>$benefit];
    }

    public  function getLeaves(){
        $leave=Leave::where('status','requested')->count();
       $leaves=Leave::where('status','requested')->get();
        return ['leave'=>$leave,'leaves'=>$leaves];
    }

    public  function checkifuserhasleave(Request $request){
        $leaves=Leave::where('employee_id',$request->employee_id)->where('status','requested')->orwhere('employee_id',$request->employee_id)->where('status','accepted')->count();
        return $leaves;
    }


    public  function getData(){
        $year=now()->year;
        $female=Employee::where('gender','Female')->count();
        $male=Employee::where('gender','Male')->count();
        $other=Employee::where('gender','!=','Male')->where('gender','!=','Female')->count();
        $benefit = DB::select( DB::raw("SELECT YEAR(DATE) AS year, MONTHNAME(DATE) AS month,MONTH(DATE) AS value,SUM(amount) AS amount
 FROM `payslip_datas` WHERE year='$year' AND name!='Basic Salary' AND type='Benefit' GROUP BY YEAR(DATE),MONTHNAME(DATE),MONTH(DATE) ORDER BY value ASC
") );
        $deduction = DB::select( DB::raw("SELECT YEAR(DATE) AS year, MONTHNAME(DATE) AS month,MONTH(DATE) AS value,SUM(amount) AS amount
 FROM `payslip_datas` WHERE YEAR='$year'  AND TYPE='Deduction' GROUP BY YEAR(DATE),MONTHNAME(DATE),MONTH(DATE) ORDER BY value ASC
") );

        $gender=array(
            'female'=>$female,
            'male'=>$male,
            'other'=>$other,
        );
        return['gender'=>$gender,'benefit'=>$benefit,'deduction'=>$deduction];

    }

    public  function getLeaveInfo(Request $request){
        $leave=MaintainedLeave::find($request->id);
        return $leave;
    }
}
