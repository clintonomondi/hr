<?php

namespace App\Http\Controllers;

use App\Payroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BasicSalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function index(){
        $basics=Payroll::all();
        return view('basic.index',compact('basics'));
    }

    public function add(){
        return view('basic.add');
    }

    public  function post(Request $request){
        $request->validate([
            'employee_id' => 'required',
        ]);
        if($request->employee_id=='0'){
            return redirect()->back()->with('error','Please Select employee');
        }
        $datas = DB::select( DB::raw("INSERT INTO payrolls(employee_id,gross)VALUES('$request->employee_id',
'$request->gross')
ON DUPLICATE KEY UPDATE gross='$request->gross'") );


        return redirect()->route('basic/index')->with('success','Information  updated successfully');
    }

    public  function update(Request $request,$id){
        $data=Payroll::findOrFail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Information  updated successfully');
    }
}
