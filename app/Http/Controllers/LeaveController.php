<?php

namespace App\Http\Controllers;

use App\Leave;
use App\MaintainedLeave;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Knox\AFT\AFT;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function index(){
        $leaves=MaintainedLeave::all();
  return view('leaves.index',compact('leaves'));
    }

    public  function pushmaintained(Request $request){
        if($request->days<=0){
            return redirect()->back()->with('error','leave days cannot be 0 or less');
        }
        $request['user_id']=Auth::user()->id;
        MaintainedLeave::create($request->all());
        return redirect()->back()->with('success','Leave  submitted successfully');
    }
    public  function updatemaintained(Request $request,$id){
        if($request->days<=0){
            return redirect()->back()->with('error','leave days cannot be 0 or less');
        }
        $leave=MaintainedLeave::find($id);
        $leave->update($request->all());
        return redirect()->back()->with('success','Leave  updated successfully');
    }
    public  function request(){

        $leaves=Leave::where('employee_id',Auth::user()->employee_id)->where('status','requested')->orwhere('employee_id',Auth::user()->employee_id)->where('status','accepted')->count();
        $leavess=MaintainedLeave::all();
        return view('leaves.request',compact('leaves','leavess'));
    }

    public  function adminrequest(){
        $leaves=MaintainedLeave::all();
        return view('leaves.admin_request',compact('leaves'));
    }

    public  function requestPost(Request $request){
    $request['created_by']=Auth::user()->id;
    $request['employee_id']=Auth::user()->employee_id;
        if(empty($request->leave_id)){
            return redirect()->back()->with('error','Please you must select  leave type');
        }
        if($request->start_date==$request->end_date){
            return redirect()->back()->with('error','Start date and end date cannot be the same');
        }
        if($request->start_date>$request->end_date){
            return redirect()->back()->with('error','End date cannot be earlier than start date');
        }
        $leave=MaintainedLeave::find($request->leave_id);
        $days=(strtotime($request->end_date)-strtotime($request->start_date))/(60 * 60 * 24);
        $balance=Leave::where('status','!=','rejected')->where('status','!=','cancelled')->where('leave_id',$request->id)->where('employee_id',$request->employee_id)->sum('balance');
        $newdays=$balance+$days;
        if($days>$leave->days){
            return redirect()->back()->with('error','You have requested more days than stated in the leave');
        }
        if($newdays>=$leave->days){
            $request['balance']=0;
        }else{
            $request['balance']=$leave->days-$newdays;
        }

        $query = DB::select(DB::raw("UPDATE leaves set balance='0' WHERE employee_id='$request->employee_id'"));

        try {
            $request['end_date'] = (new Carbon($request->start_date))->addDays($newdays);
            $request['start_date']= (new Carbon($request->start_date));
        } catch (\Exception $e) {
        }
        $request['days']=$newdays;
        $request['year']=now()->year;
        $request['created_by']=Auth::user()->id;
        $date=Leave::create($request->all());
        return redirect()->back()->with('success','Leave requested submitted successfully');
    }

    public  function adminPostrequest(Request $request){
        if($request->start_date==$request->end_date){
            return redirect()->back()->with('error','Start date and end date cannot be the same');
        }
        if($request->start_date>$request->end_date){
            return redirect()->back()->with('error','End date cannot be earlier than start date');
        }
        if(empty($request->employee_id)  || $request->employee_id=='0' || empty($request->leave_id)){
            return redirect()->back()->with('error','Please you must select staff and leave type');
        }
        $leave=MaintainedLeave::find($request->leave_id);
        $days=(strtotime($request->end_date)-strtotime($request->start_date))/(60 * 60 * 24);
        $balance=Leave::where('status','!=','rejected')->where('status','!=','cancelled')->where('leave_id',$request->leave_id)->where('employee_id',$request->employee_id)->sum('balance');
       $newdays=$balance+$days;
       if($days>$leave->days){
           return redirect()->back()->with('error','You have requested more days than stated in the leave');
       }
       if($newdays>=$leave->days){
           $request['balance']=0;
       }else{
           $request['balance']=$leave->days-$newdays;
       }

       $query = DB::select(DB::raw("UPDATE leaves set balance='0' WHERE employee_id='$request->employee_id'"));

        try {
            $request['end_date'] = (new Carbon($request->start_date))->addDays($newdays);
            $request['start_date']= (new Carbon($request->start_date));
        } catch (\Exception $e) {
        }
        $request['days']=$newdays;
        $request['year']=now()->year;
        $request['created_by']=Auth::user()->id;
        $date=Leave::create($request->all());
        return redirect()->back()->with('success','Leave requested submitted successfully');
    }

    public  function adminleaves(){
        $leaves=Leave::orderBy('id','desc')->get();
        return view('leaves.admin_leaves',compact('leaves'));
    }
    public  function myleaves(){
        $leaves=Leave::orderBy('id','desc')->where('employee_id',Auth::user()->employee->id)->get();
        return view('leaves.my_leaves',compact('leaves'));
    }

    public  function changeLeave(Request $request){
        $leave=Leave::findorFail($request->id);
        $request['updated_by']=Auth::user()->id;
        $phone="254".substr($leave->employee->phone, 1);;
        $name=$leave->employee->fname;

        if($request->status=='accepted'){
            AFT::sendMessage($phone, 'Hello! '.$name.' your  '.$leave->type.'  leave request has been  accepted. Your leave starts on '.$leave->start_date.' and ends on '.$leave->end_date.'.Enjoy! .@TRIGALS');
        }
        elseif($request->status=='rejected'){
            AFT::sendMessage($phone, 'Sorry! '.$name.', this is to inform you that  your '.$leave->type.'  leave request has been  rejected. Please login to see more. @TRIGALS');
        }else{

        }

        $leave->update($request->all());
       return ['status'=>true,'message','Leave updated successfully','data'=>$leave->employee->phone];
    }


    public  function view($id){
        $leave=Leave::find($id);
        return view('leaves.view',compact('leave'));
    }

    public  function recall(Request $request,$id){
        if($request->remainder<=0){
            return redirect()->back()->with('error','This leave cannot be cancelled');
        }
      $leave=Leave::find($id);
        $request['balance']=$request->remainder+$leave->balance;
        $request['status']='recalled';
        $request['updated_by']=Auth::user()->id;
        $leave->update($request->all());
        AFT::sendMessage($leave->employee->phone, 'Sorry! '.$leave->employee->fname.', this is to inform you that  your '.$leave->leave->name.' has been RECALLED.Balance of '.$request->balance.'days  will be carried forward @TRIGALS');
        return redirect()->back()->with('success','This leave has been recalled successfully');
    }
}
