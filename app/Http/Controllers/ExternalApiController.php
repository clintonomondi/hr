<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ExternalApiController extends Controller
{
    public static  function sendSMS($phone,$message){
        $phone_to_send='254'.substr($phone,1);
        $client = new Client();
        $response = $client->get('https://sms.damzaltd.com/sms/api?action=send-sms&api_key=dHJpZ2FsczpwYXNzd29yZA==&to='.$phone_to_send.'&from=Trigals_Ltd&sms='.$message);
        return $response;
    }
}
