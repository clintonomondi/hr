<?php

namespace App\Http\Controllers;

use App\MaintainedDeduction;
use App\NHIF;
use App\NSSF;
use App\Paye;
use Illuminate\Http\Request;

class DeductionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function paye(){
        $payes=Paye::all();
     return view('paye.index',compact('payes'));
   }
   public  function nhif(){
        $nhifs=NHIF::all();
     return view('nhif.index',compact('nhifs'));
   }
   public  function nssf(){
        $nssfs=NSSF::all();
     return view('nssf.index',compact('nssfs'));
   }

   public  function addPaye(Request $request){
       if(empty($request->to_amount) || empty($request->from_amount) || empty($request->percentage)){
           return redirect()->back()->with('error','You must add new range to submit');
       }
       $count = count($request->to_amount);
       for($i = 0; $i < $count; $i++) {
           if(empty($request->to_amount[$i]) || empty($request->from_amount[$i]) || empty($request->percentage[$i])){

           }
           $data=array(
               'to_amount'=>$request->to_amount[$i],
               'from_amount'=>$request->from_amount[$i],
               'percentage'=>$request->percentage[$i],
           );
           Paye::insert($data);
       }
       return redirect()->back()->with('success','Paye data inserted successfully');

   }
   public  function addNhif(Request $request){
       if(empty($request->to_amount) || empty($request->from_amount) || empty($request->amount)){
           return redirect()->back()->with('error','You must add new range to submit');
       }
       $count = count($request->to_amount);
       for($i = 0; $i < $count; ++$i) {
           if(empty($request->to_amount[$i]) || empty($request->from_amount[$i]) || empty($request->amount[$i])){

           }
           $data=array(
               'to_amount'=>$request->to_amount[$i],
               'from_amount'=>$request->from_amount[$i],
               'amount'=>$request->amount[$i],
           );
           NHIF::insert($data);
       }
       return redirect()->back()->with('success','NHIF data inserted successfully');

   }
   public  function addNssf(Request $request){
       if(empty($request->to_amount) || empty($request->from_amount) || empty($request->amount)){
           return redirect()->back()->with('error','You must add new range to submit');
       }
       $count = count($request->to_amount);
       for($i = 0; $i < $count; ++$i) {
           if(empty($request->to_amount[$i]) || empty($request->from_amount[$i]) || empty($request->amount[$i])){

           }
           $data=array(
               'to_amount'=>$request->to_amount[$i],
               'from_amount'=>$request->from_amount[$i],
               'amount'=>$request->amount[$i],
           );
           NSSF::insert($data);
       }
       return redirect()->back()->with('success','NSSF data inserted successfully');

   }

   public  function removePayeItem($id){
        $data=Paye::findorFail($id);
        $data->delete();
       return redirect()->back()->with('success','Paye data removed successfully');
   }
   public  function removeNhifItem($id){
        $data=NHIF::findorFail($id);
        $data->delete();
       return redirect()->back()->with('success','NHIF data removed successfully');
   }
   public  function removeNssfItem($id){
        $data=NSSF::findorFail($id);
        $data->delete();
       return redirect()->back()->with('success','NSSF data removed successfully');
   }

   public  function updatePaye(Request $request,$id){
        $data=Paye::findorfail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Data updated successfully');
   }
   public  function updateNHIF(Request $request,$id){
       $data=NHIF::findorfail($id);
       $data->update($request->all());
       return redirect()->back()->with('success','Data updated successfully');
   }
    public  function updateNSSF(Request $request,$id){
        $data=NSSF::findorfail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Data updated successfully');
    }


    public  function maintained(){
        $allowances=MaintainedDeduction::all();
        return view('maintained.mainatineddeduction',compact('allowances'));
    }

    public  function pushmaintained(Request $request){
        $request->validate([
            'name' => 'required',
        ]);
        $data=MaintainedDeduction::create($request->all());
        return redirect()->back()->with('success','Deduction data updated successfully');
    }

    public  function updatemaintained(Request $request,$id){
        $data=MaintainedDeduction::findorFail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Deduction data updated successfully');
    }
}
