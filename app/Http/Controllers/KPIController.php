<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Kpi;
use App\Performance;
use App\Performance_data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function maintained(){
        $kpis=Kpi::all();
        return view('performance.index',compact('kpis'));
    }

    public  function push(Request $request){
        $request['user_id']=Auth::user()->id;
        $data=Kpi::create($request->all());
        return redirect()->back()->with('success','KPI maintained successfully');
    }

    public  function update(Request $request,$id){
        $data=Kpi::findorFail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','KPI updated successfully');
    }

    public  function performance(){
        $kpis=Performance::where('status','Active')->get();
        $indicators=Kpi::all();
        return view('performance.performance',compact('kpis','indicators'));
    }

    public  function myperformance(){
        $kpis=Performance::where('employee_id',Auth::user()->employee_id)->get();
        return view('performance.myperformance',compact('kpis'));
    }

    public  function performanceGetByStaff(Request $request){
        $indicators=Kpi::all();
        if(!empty($request->kpi_id)){
            $kpis = Performance::where('status', 'Active')->where('kpi_id', $request->kpi_id)->get();
        }else {
            $user = Employee::find($request->id);
            $kpis = Performance::where('status', 'Active')->where('employee_id', $request->id)->get();
        }
        return view('performance.performance',compact('kpis','user','indicators'));
    }

    public  function inactive(){
        $kpis=Performance::where('status','Inactive')->get();
        return view('performance.previous',compact('kpis'));
    }

    public function inactiveGetBy(Request $request){
        $user=Employee::find($request->id);
        $kpis=Performance::where('status','Inactive')->where('employee_id',$request->id)->get();
        return view('performance.previous',compact('kpis','user'));
    }

    public  function add(){
        $kpis=Kpi::all();
        return view('performance.addcycle',compact('kpis'));
    }

    public  function pushcycle(Request $request){
        if($request->employee_id=='0'){
            return redirect()->back()->with('errors','Select Staff');
        }

       $data=Performance::create($request->all());
        return redirect()->back()->with('success','Cycle added successfully');
    }

    public  function record(Request $request){
      $performance_datas=Performance_data::where('performance_id',$request->performance_id)->count();
      if($performance_datas>=5){
          return redirect()->back()->with('errors','Record cannot exceed five weeks records');
      }
      if($performance_datas<1){
          $request['week']=1;
      }else{
          $request['week']=$performance_datas+1;
      }
        $data=Performance_data::create($request->all());
        return redirect()->back()->with('success','Record added successfully');
    }

    public  function records($id){
        $kpis=Performance_data::where('performance_id',$id)->get();
        $data=Performance::find($id);
        return view('performance.records',compact('kpis','data'));
    }

    public  function activate(Request $request,$id){
        $request['status']='Active';
        $data=Performance::find($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Record updated successfully');
    }
    public  function inactivate(Request $request,$id){
        $request['status']='Inactive';
        $data=Performance::find($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Record update successfully');
    }
}
