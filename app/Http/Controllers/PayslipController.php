<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Deduction;
use App\DeductionLog;
use App\Employee;
use App\Jobs\SendPayslipEmail;
use App\Mail\PayslipEmail;
use App\MaintainedDeduction;
use App\NHIF;
use App\NSSF;
use App\Paye;
use App\Payroll;
use App\Payslip;
use App\Payslip_data;
use App\PersionalRelief;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class PayslipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $payslips = DB::select(DB::raw("SELECT month AS month ,year AS year,date as date,status as status,batch_id as batch_id,
(SELECT sum(amount) from `payslip_datas` B where A.batch_id=B.batch_id and type='Benefit')netamount,
(SELECT sum(amount) from `payslip_datas` B where A.batch_id=B.batch_id and type='deduction')deductionamount,
(SELECT COUNT(*) FROM `payslips` B WHERE A.date=B.date AND STATUS='unapproved')totalunapproved,
(SELECT COUNT(*) FROM `payslips` B WHERE A.batch_id=B.batch_id AND STATUS='approved')totalapproved,
(SELECT COUNT(*) FROM `payslips` B WHERE A.batch_id=B.batch_id AND STATUS='rejected')rejected,
(SELECT COUNT(*) FROM `payslips` B WHERE A.batch_id=B.batch_id)total
 FROM payslips A WHERE status!='rejected' GROUP BY batch_id,month,year,date,status ORDER BY date desc"));

        return view('payslip.index', compact('payslips'));
    }

    public function generate()
    {
        $users = Employee::where('status', 'Active')->get();
        return view('payslip.generate', compact('users'));
    }

    public function push(Request $request)
    {
        $request->validate([
            'date' => 'required',
        ]);
        if (empty($request->employee_id) || $request->employee_id == '0') {
            return redirect()->back()->with('error', 'Please select at least one user');
        }
        $year = Carbon::createFromFormat('Y-m-d', $request->date)->year;
        $month = Carbon::createFromFormat('Y-m-d', $request->date)->month;
        $request['year'] = $year;
        $request['month'] = $month;
        $request['created_by'] = Auth::user()->id;
        $request['batch_id'] =$request->date.'_'.mt_rand(100000, 999999);
        foreach ($request->employee_id as $key => $employee_id) {
            $checkuser = Payslip::where('employee_id', $employee_id)->where('month', $month)->where('year', $year)->where('status','!=','rejected')->count();

            if ($checkuser != 0) {

            } else {
                $user = Employee::find($employee_id);
                $request['employee_id'] = $employee_id;
                $request['payslip_no'] = $user->regno . '' . $month . '' . $year;
                $insert = Payslip::create($request->all());

                $total_benefit = Benefit::where('employee_id', $employee_id)->where('months','!=','0')->sum('amount');
                $basic = Payroll::where('employee_id', $employee_id)->sum('gross');
                $tax_relief = PersionalRelief::sum('amount');
                $gross = ($total_benefit + $basic);



                $nhifQuery = NHIF::whereRaw('? between from_amount and to_amount', [$basic])->first();
                $nssfQuery = NSSF::whereRaw('? between from_amount and to_amount', [$basic])->first();
                $pension_id=MaintainedDeduction::where('name','Pension Fund')->first();
                $insurance_id=MaintainedDeduction::where('name','Insurance Premium')->first();

                if(!empty($pension_id)){
                    $pension=Deduction::where('employee_id',$employee_id)->where('deduction_id',$pension_id->id)->where('balance','!=','0')->sum('amount_deducted');
                }else{
                    $pension=0;
                }
                if(!empty($insurance_id)){
                    $insurance=Deduction::where('employee_id',$employee_id)->where('deduction_id',$insurance_id->id)->where('balance','!=','0')->sum('amount_deducted');
                }else{
                    $insurance=0;
                }

                if (empty($nhifQuery)) {
                    $nhif = '0';
                } else {
                    $nhif = $nhifQuery->amount;
                }

                if (empty($nssfQuery)) {
                    $nssf = '0';
                } else {
                    $nssf = $nssfQuery->amount;
                }
                $TI=$gross-$nssf-$pension;
                if($TI>24000){
                    $result =$this->calculatePayer($TI);
                }else{
                    $result=0;
                }

                if ($result==0) {
                    $paye = 0;
                } else {
                        $calc=(15/100) * $insurance;
                        if($calc>=5000){
                            $insrance_relief=5000;
                        }else{
                            $insrance_relief=$calc;
                        }

                    $paye = $result - $tax_relief-$insrance_relief;
                }


               ////Deduction and benefit Removed from here
                ///
                $other = array(
                    array(
                        "payslip_id" => $insert->id,
                        "type" => 'Deduction',
                        "amount" => $paye,
                        "name" => 'PAYE',
                        "month" => $month,
                        "year" => $year,
                        "date" => $request->date,
                        "batch_id" => $request->batch_id,
                        "employee_id" =>$employee_id,
                    ),
                    array(
                        "payslip_id" => $insert->id,
                        "type" => 'Deduction',
                        "amount" => $nhif,
                        "name" => 'NHIF',
                        "month" => $month,
                        "year" => $year,
                        "date" => $request->date,
                        "batch_id" => $request->batch_id,
                        "employee_id" =>$employee_id,
                    ),
                    array(
                        "payslip_id" => $insert->id,
                        "type" => 'Deduction',
                        "amount" => $nssf,
                        "name" => 'NSSF',
                        "month" => $month,
                        "year" => $year,
                        "date" => $request->date,
                        "batch_id" => $request->batch_id,
                        "employee_id" =>$employee_id,
                    ),
                    array(

                        "payslip_id" => $insert->id,
                        "type" => 'Benefit',
                        "amount" => $basic,
                        "name" => 'Basic Salary',
                        "month" => $month,
                        "year" => $year,
                        "date" => $request->date,
                        "batch_id" => $request->batch_id,
                        "employee_id" =>$employee_id,
                    ),
                    array(

                        "payslip_id" => $insert->id,
                        "type" => 'Benefit',
                        "amount" => $tax_relief,
                        "name" => 'Personal Relief',
                        "month" => $month,
                        "year" => $year,
                        "date" => $request->date,
                        "batch_id" => $request->batch_id,
                        "employee_id" =>$employee_id,
                    ),
                );
                Payslip_data::insert($other);
                unset($other);

            }
        }

        return redirect()->route('payslip/index')->with('success', 'Payslips generated successfully');


    }

    public static function calculatePayer($TI){
        $balance=$TI;
        $paye=0;
        $ranges=Paye::all();
        $count=1;
        foreach ($ranges as $range) {
            if ($balance > 0) {
                if ($count == $ranges->count()) {
                    $tax = ($balance * $range->percentage) / 100;
                    $paye = $paye + $tax;
                    $balance = $balance - $balance;
                } else {
                $diff = $range->to_amount - $range->from_amount;
                if ($balance > $diff) {
                    $tax = ($diff * $range->percentage) / 100;
                    $paye = $paye + $tax;
                    $balance = $balance - $diff;
                } else {
                    $tax = ($balance * $range->percentage) / 100;
                    $paye = $paye + $tax;
                    $balance = $balance - $balance;
                }
            }
            }
            $count++;
        }
      return $paye;
    }


    public function payslips($date)
    {
        $datas = Payslip::where('batch_id', $date)->where('status','!=','rejected')->get();
        foreach ($datas as $data) {
            $basic = Payslip_data::where('payslip_id', $data->id)->where('name', 'Basic Salary')->sum('amount');
            $tax_relief = Payslip_data::where('payslip_id', $data->id)->where('name', 'Personal Relief')->sum('amount');
            $paye = Payslip_data::where('payslip_id', $data->id)->where('name', 'PAYE')->sum('amount');
            $nhif = Payslip_data::where('payslip_id', $data->id)->where('name', 'NHIF')->sum('amount');
            $nssf = Payslip_data::where('payslip_id', $data->id)->where('name', 'NSSF')->sum('amount');
            $benefits = Payslip_data::where('payslip_id', $data->id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
            $deduction = Payslip_data::where('payslip_id', $data->id)->where('type', 'Deduction')->where('name', '!=', 'NHIF')->where('name', '!=', 'NSSF')->where('name', '!=', 'PAYE')->sum('amount');
            $ti = ($basic + $benefits);
            $net = $ti-$paye-$deduction-$nhif-$nssf;
            $payslips[] = array(
                "name" => $data->employee->fname . '  ' . $data->employee->lname,
                "regno" => $data->employee->regno,
                "basic" => $basic,
                "payslip_no" => $data->payslip_no,
                "paye" => $paye,
                "nhif" => $nhif,
                "nssf" => $nssf,
                "benefit" => $benefits,
                "deductions" => $deduction,
                "net" => $net,
                "id" => $data->id,
                "tax_relief" =>$tax_relief,
            );
        }
//        $year = Carbon::createFromFormat('Y-m-d', $date)->year;
//        $month = Carbon::createFromFormat('Y-m-d', $date)->month;
        $batch_id=$date;
        return view('payslip.payslips', compact('batch_id', 'payslips'));
    }

    public function print($date)
    {
        $datas = Payslip::where('batch_id', $date)->where('status','!=','rejected')->get();
        $date = Payslip::where('batch_id', $date)->where('status','!=','rejected')->first();
        foreach ($datas as $data) {
            $basic = Payslip_data::where('payslip_id', $data->id)->where('name', 'Basic Salary')->sum('amount');
            $tax_relief = Payslip_data::where('payslip_id', $data->id)->where('name', 'Personal Relief')->sum('amount');
            $paye = Payslip_data::where('payslip_id', $data->id)->where('name', 'PAYE')->sum('amount');
            $nhif = Payslip_data::where('payslip_id', $data->id)->where('name', 'NHIF')->sum('amount');
            $nssf = Payslip_data::where('payslip_id', $data->id)->where('name', 'NSSF')->sum('amount');
            $benefits = Payslip_data::where('payslip_id', $data->id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
            $deduction = Payslip_data::where('payslip_id', $data->id)->where('type', 'Deduction')->where('name', '!=', 'NHIF')->where('name', '!=', 'NSSF')->where('name', '!=', 'PAYE')->sum('amount');
            $ti = ($basic + $benefits);
            $net = $ti-$paye-$deduction-$nhif-$nssf;
            $payslips[] = array(
                "name" => $data->employee->fname . '  ' . $data->employee->lname,
                "regno" => $data->employee->regno,
                "basic" => $basic,
                "payslip_no" => $data->payslip_no,
                "paye" => $paye,
                "nhif" => $nhif,
                "nssf" => $nssf,
                "benefit" => $benefits,
                "deductions" => $deduction,
                "net" => $net,
                "id" => $data->id,
                "tax_relief" =>$tax_relief,
            );
        }
        $batch_id=$date;
        return view('payslip.print', compact('batch_id', 'payslips','date'));
    }

    public  function rejected($date){
        $datas = Payslip::where('batch_id', $date)->where('status','rejected')->get();
        foreach ($datas as $data) {
            $basic = Payslip_data::where('payslip_id', $data->id)->where('name', 'Basic Salary')->sum('amount');
            $tax_relief = Payslip_data::where('payslip_id', $data->id)->where('name', 'Personal Relief')->sum('amount');
            $paye = Payslip_data::where('payslip_id', $data->id)->where('name', 'PAYE')->sum('amount');
            $nhif = Payslip_data::where('payslip_id', $data->id)->where('name', 'NHIF')->sum('amount');
            $nssf = Payslip_data::where('payslip_id', $data->id)->where('name', 'NSSF')->sum('amount');
            $benefits = Payslip_data::where('payslip_id', $data->id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
            $deduction = Payslip_data::where('payslip_id', $data->id)->where('type', 'Deduction')->where('name', '!=', 'NHIF')->where('name', '!=', 'NSSF')->where('name', '!=', 'PAYE')->sum('amount');
            $ti = ($basic + $benefits);
            $net = $ti-$paye-$deduction-$nhif-$nssf;
            $payslips[] = array(
                "name" => $data->employee->fname . '  ' . $data->employee->lname,
                "regno" => $data->employee->regno,
                "basic" => $basic,
                "payslip_no" => $data->payslip_no,
                "remarks" => $data->remarks,
                "paye" => $paye,
                "nhif" => $nhif,
                "nssf" => $nssf,
                "benefit" => $benefits,
                "deductions" => $deduction,
                "net" => $net,
                "id" => $data->id,
                "tax_relief" =>$tax_relief,
            );
        }
//        $year = Carbon::createFromFormat('Y-m-d', $date)->year;
//        $month = Carbon::createFromFormat('Y-m-d', $date)->month;
        $batch_id=$date;

        return view('payslip.rejected', compact('batch_id', 'payslips'));
    }

    public  function mypayslips(){
        $datas = Payslip::orderBy('id','desc')->where('employee_id', Auth::user()->employee->id)->where('status','approved')->get();
        foreach ($datas as $data) {
            $basic = Payslip_data::where('payslip_id', $data->id)->where('name', 'Basic Salary')->sum('amount');
            $tax_relief = Payslip_data::where('payslip_id', $data->id)->where('name', 'Personal Relief')->sum('amount');
            $paye = Payslip_data::where('payslip_id', $data->id)->where('name', 'PAYE')->sum('amount');
            $nhif = Payslip_data::where('payslip_id', $data->id)->where('name', 'NHIF')->sum('amount');
            $nssf = Payslip_data::where('payslip_id', $data->id)->where('name', 'NSSF')->sum('amount');
            $benefits = Payslip_data::where('payslip_id', $data->id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
            $deduction = Payslip_data::where('payslip_id', $data->id)->where('type', 'Deduction')->where('name', '!=', 'NHIF')->where('name', '!=', 'NSSF')->where('name', '!=', 'PAYE')->sum('amount');
            $ti = ($basic + $benefits);
            $net = $ti-$paye-$deduction-$nhif-$nssf;
            $payslips[] = array(
                "name" => $data->employee->fname . '  ' . $data->employee->lname,
                "regno" => $data->employee->regno,
                "basic" => $basic,
                "payslip_no" => $data->payslip_no,
                "paye" => $paye,
                "nhif" => $nhif,
                "nssf" => $nssf,
                "benefit" => $benefits,
                "deductions" => $deduction,
                "net" => $net,
                "id" => $data->id,
                "tax_relief" =>$tax_relief,
                "month" =>$data->month,
                "year" =>$data->year,
            );
        }

        return view('payslip.mypayslips', compact( 'payslips'));
    }

    public function view($id)
    {
        $payslip = Payslip::findOrFail($id);

        $benefits = Payslip_data::where('payslip_id', $id)->where('type', 'Benefit')->where('name','!=','Personal Relief')->orderBy('name', 'asc')->get();
        $nssf = Payslip_data::where('payslip_id', $id)->where('name','NSSF')->sum('amount');
        $deductions = Payslip_data::where('payslip_id', $id)->where('type', 'Deduction')->get();
        $basic = Payslip_data::where('payslip_id', $id)->where('name', 'Basic Salary')->sum('amount');
        $benefit = Payslip_data::where('payslip_id', $id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
        $ti = ($basic + $benefit);
        $net = $ti-$deductions->sum('amount');

        return view('payslip.view', compact('payslip', 'deductions', 'benefits', 'net','ti'));

    }

    public  function approve(Request $request){
        $user=Auth::user()->id;
        if (empty($request->payslip_no)) {
            return ['status'=>false,'message'=>'Please select at least one payslip'];
        }
        foreach ($request->payslip_no as $payslip_no){
            $insert=Payslip::where('payslip_no',$payslip_no)->where('status','unapproved')->first();
            if(empty($insert)){

            }else {


                $payslips = DB::select(DB::raw("UPDATE payslips SET status='approved',approved_by='$user' WHERE status='unapproved' and payslip_no='$payslip_no'"));
                $total_benefitarray = Benefit::where('employee_id', $insert->employee_id)->where('months', '!=', '0')->get();
                $total_deduction = Deduction::where('employee_id', $insert->employee_id)->where('balance', '>', '0')->get();

                if (!$total_benefitarray->isEmpty()) {
                    foreach ($total_benefitarray as $benefit) {
                        $newbenefit = Benefit::find($benefit->id);
                        $newmonths = $benefit->months - 1;
                        $request['months'] = $newmonths;
                        $newbenefit->update($request->all());
                        $bene[] = array(
                            "payslip_id" => $insert->id,
                            "type" => "Benefit",
                            "amount" => $benefit->amount,
                            "name" => $benefit->allowance->name,
                            "benefit_id" => $benefit->allowance_id,
                            "month" => $insert->month,
                            "year" => $insert->year,
                            "date" => $insert->date,
                            "batch_id" => $insert->batch_id,
                            "employee_id" => $insert->employee_id,
                        );

                    }
                    Payslip_data::insert($bene);
                    unset($bene);
                } else {

                }

                if (!$total_deduction->isEmpty()) {
                    foreach ($total_deduction as $eduction) {
                        if ($eduction->balance < $eduction->amount_deducted) {
                            $amount = $eduction->balance;
                            $balance = 0;
                        } else {
                            $amount = $eduction->amount_deducted;
                            $balance = ($eduction->balance) - ($eduction->amount_deducted);
                        }
                        $dedu[] = array(
                            "payslip_id" => $insert->id,
                            "type" => 'Deduction',
                            "amount" => $amount,
                            "name" => $eduction->deduction->name,
                            "deduction_id" => $eduction->deduction_id,
                            "month" => $insert->month,
                            "year" => $insert->year,
                            "date" => $insert->date,
                            "batch_id" => $insert->batch_id,
                            "employee_id" => $insert->employee_id,
                        );
                        $query = DB::select(DB::raw("UPDATE deductions SET balance='$balance' WHERE id='$eduction->id'"));

                        $deductionLog[] = array(
                            "payslip_id" => $insert->id,
                            "amount" => $amount,
                            "deduction_id" => $eduction->id,
                            "date" => now(),
                            "batch_id" => $insert->batch_id,
                            "employee_id" => $insert->employee_id,
                        );
                    }
                    Payslip_data::insert($dedu);
                    DeductionLog::insert($deductionLog);
                    unset($dedu);
                    unset($deductionLog);
                } else {

                }
            }

        }
        $unapproveds=Payslip::where('batch_id',$request->batch_id)->where('status','unapproved')->get();
        if($unapproveds->isEmpty()){

        }else{
            foreach ($unapproveds as $payslip){
                $slip=Payslip::find($payslip->id);
                $data=DB::table('payslip_datas')->where('payslip_id', $payslip->id)->delete();
                $slip->delete();
            }
        }


        return ['status'=>true,'message'=>'Information updated successfully'];
    }

    public  function reject(Request $request){


        $user=Auth::user()->id;
        if (empty($request->payslip_no)) {
            return ['status'=>false,'message'=>'Please select at least one payslip'];
        }
        foreach ($request->payslip_no as $payslip_no){
            $payslip=Payslip::where('status','unapproved')->where('payslip_no',$payslip_no)->first();
            if(empty($payslip)){

            }else {
                $slip = Payslip::find($payslip->id);
                $data = DB::table('payslip_datas')->where('payslip_id', $payslip->id)->delete();
                $slip->remarks=$request->remarks;
                $slip->status=$request->status;
                $slip->update();
            }
        }
        return ['status'=>true,'message'=>'Information updated successfully'];

    }

    public  function sendPayslipEmail(Request $request){
        $slips=Payslip::where('batch_id',$request->batch_id)->where('status','approved')->get();
        if(empty($slips)){
            return ['status'=>false,'message'=>'We did not find any data in this category'];
        }
        SendPayslipEmail::dispatch($slips);
        return ['status'=>true,'message'=>'Email sent successfully'];
    }

    public  function userview($id){

        $payslip = Payslip::findOrFail($id);
        $benefits = Payslip_data::where('payslip_id', $id)->where('type', 'Benefit')->where('name','!=','Personal Relief')->orderBy('name', 'asc')->get();
        $deductions = Payslip_data::where('payslip_id', $id)->where('type', 'Deduction')->get();
        $basic = Payslip_data::where('payslip_id', $id)->where('name', 'Basic Salary')->sum('amount');
        $benefit = Payslip_data::where('payslip_id', $id)->where('type', 'Benefit')->where('name', '!=', 'Basic Salary')->where('name','!=','Personal Relief')->sum('amount');
        $ti = ($basic + $benefit);
        $net = $ti-$deductions->sum('amount');
        return view('payslip.userview', compact('payslip', 'deductions', 'benefits', 'net','ti'));
    }



}
