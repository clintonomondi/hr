<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Deduction;
use App\NHIF;
use App\NSSF;
use App\Paye;
use App\Payroll;
use App\PersionalRelief;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReliefController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function personal(){
        $personal=PersionalRelief::first();
        return view('relief.personalrelief',compact('personal'));
    }

    public  function post (Request $request){
        $request['name']='Personal Relief';
        $data=PersionalRelief::create($request->all());
        return redirect()->back()->with('success','Information updated successfully');
    }

    public  function update(Request $request,$id){
        $data=PersionalRelief::findOrFail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Information updated successfully');
    }

    public  function preview(){
        $personal=PersionalRelief::all();
       $basic=Payroll::where('employee_id',Auth::user()->employee->id)->sum('gross');

        $total_benefit = Benefit::where('employee_id',Auth::user()->employee->id)->sum('amount');
        $benefits = Benefit::where('employee_id',Auth::user()->employee->id)->get();
        $deductions = Deduction::where('employee_id',Auth::user()->employee->id)->get();
        $TI = ($total_benefit + $basic);


        $result = Paye::whereRaw('? between from_amount and to_amount',[$TI])->first();
        $nhifQuery = NHIF::whereRaw('? between from_amount and to_amount',[$basic])->first();
        $nssfQuery = NSSF::whereRaw('? between from_amount and to_amount',[$basic])->first();
        if (empty($result)) {
            $paye = '0';
        } else {
            $paye = (($result->percentage * $TI) / 100)-$personal->sum('amount');
        }
        if (empty($nhifQuery)) {
            $nhif = '0';
        } else {
            $nhif = $nhifQuery->amount;
        }

        if (empty($nssfQuery)) {
            $nssf = '0';
        } else {
            $nssf = $nssfQuery->amount;
        }
        $net = $TI-$paye-$deductions->sum('amount_deducted')-$nhif-$nssf;

        return view('payslip.myPayslipPreview',compact('personal','net','basic','paye','total_benefit','TI','benefits','deductions','nhif','nssf'));
    }
}
