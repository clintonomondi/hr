<?php

namespace App\Http\Controllers;

use App\Leave;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data=User::where('email',Auth::user()->email)->orWhere('phone',Auth::user()->phone)->first();
        if($data->status!='Active'){
            return redirect('login')->with(Auth::logout())->with('error','You are not authorised to login!');
        }


        $activeusers=User::where('status','Active')->count();
        $inactiveusers=User::where('status','Inctive')->count();
        $leaves=Leave::where('status','requested')->count();


        return view('home',compact('activeusers','inactiveusers','leaves'));
    }

    public  function profile(){
        $user=User::find(Auth::user()->id)->employee;
        return view('profile',compact('user'));
    }

    public function postpassword(Request $request)
    {

        $this->validate($request, [
            'currentpass' => 'required|min:5',
            'newpass' => 'required|min:5',
            'repass' => 'required',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return redirect()->back()->with('errors', 'The current password is wrong');
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            return redirect()->back()->with('error', 'The new passwords do not match');
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();

        $message='Your password for Trigals System has been successfully changed.If this is not you, please contact admin';
        ExternalApiController::sendSMS(auth()->user()->phone,$message);
        return redirect()->back()->with('success', 'Password changed successfully');

    }

}
