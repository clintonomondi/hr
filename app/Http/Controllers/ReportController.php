<?php

namespace App\Http\Controllers;

use App\MaintainedAllowance;
use App\MaintainedDeduction;
use App\Payslip_data;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
public  function paye(){
   $datas=[];
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
    return view('reports.paye',compact('datas','years'));
}
    public  function nhif(){
        $datas=[];
        $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
        return view('reports.nhif',compact('datas','years'));
    }
    public  function nssf(){
        $datas=[];
        $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
        return view('reports.nssf',compact('datas','years'));
    }

public function postPayeReport(Request $request){
    if($request->year) {
        $datas = Payslip_data::where('year', $request->year)->where('name', 'PAYE')->get();
    }
    if($request->id) {
        $datas = Payslip_data::where('employee_id', $request->id)->where('name', 'PAYE')->get();
    }
    if($request->date){
        $datas=Payslip_data::where('date',$request->date)->where('name','PAYE')->get();
        $year = Carbon::createFromFormat('Y-m-d', $request->date)->year;
        $month = Carbon::createFromFormat('Y-m-d', $request->date)->month;
    }
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );

 return view('reports.paye',compact('datas','month','year','years'));

}

public function postnhifReport(Request $request){
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
    if($request->year) {
        $datas=Payslip_data::where('year',$request->year)->where('name','NHIF')->get();
    }
    if($request->id) {
        $datas=Payslip_data::where('employee_id',$request->id)->where('name','NHIF')->get();
    }
    if($request->date) {
        $datas=Payslip_data::where('date',$request->date)->where('name','NHIF')->get();
        $year = Carbon::createFromFormat('Y-m-d', $request->date)->year;
        $month = Carbon::createFromFormat('Y-m-d', $request->date)->month;
    }

    return view('reports.nhif',compact('datas','month','year','years'));
}

public  function postnssfReport(Request $request){
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
    if($request->year) {
        $datas=Payslip_data::where('year',$request->year)->where('name','NSSF')->get();
    }
    if($request->id) {
        $datas=Payslip_data::where('employee_id',$request->id)->where('name','NSSF')->get();
    }
    if($request->date) {
        $datas=Payslip_data::where('date',$request->date)->where('name','NSSF')->get();
        $year = Carbon::createFromFormat('Y-m-d', $request->date)->year;
        $month = Carbon::createFromFormat('Y-m-d', $request->date)->month;
    }

    return view('reports.nssf',compact('datas','month','year','years'));
}

public  function deduction(){
    $datas=[];
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
    $deductions=MaintainedDeduction::all();
    return view('reports.deduction',compact('years','deductions','datas'));
}
    public  function allowance(){
        $datas=[];
        $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
        $deductions=MaintainedAllowance::all();
        return view('reports.allowance',compact('years','deductions','datas'));
    }

public  function getdeduction(Request $request){
    if(empty($request->deduction_id)){
        return redirect()->back()->with('error','Please select deduction');
    }
    if($request->date){
        $datas=Payslip_data::where('date',$request->date)->where('deduction_id',$request->deduction_id)->get();
    }
    if($request->year){
        $datas=Payslip_data::where('year',$request->year)->where('deduction_id',$request->deduction_id)->get();
    }
    if($request->id){
        $datas=Payslip_data::where('employee_id',$request->id)->where('deduction_id',$request->deduction_id)->get();
    }
    $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
    $deductions=MaintainedDeduction::all();

    return view('reports.deduction',compact('years','deductions','datas'));
}

    public  function getallowance(Request $request){
        if(empty($request->benefit_id)){
            return redirect()->back()->with('error','Please select Allowance');
        }
        if($request->date){
            $datas=Payslip_data::where('date',$request->date)->where('benefit_id',$request->benefit_id)->get();
        }
        if($request->year){
            $datas=Payslip_data::where('year',$request->year)->where('benefit_id',$request->benefit_id)->get();
        }
        if($request->id){
            $datas=Payslip_data::where('employee_id',$request->id)->where('benefit_id',$request->benefit_id)->get();
        }
        $years = DB::select( DB::raw("SELECT year as year FROM payslip_datas GROUP  BY year") );
        $deductions=MaintainedAllowance::all();

        return view('reports.allowance',compact('years','deductions','datas'));
    }
}
