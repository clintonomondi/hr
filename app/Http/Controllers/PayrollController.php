<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Deduction;
use App\DeductionLog;
use App\Employee;
use App\MaintainedDeduction;
use App\NHIF;
use App\NSSF;
use App\Paye;
use App\Payroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayrollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function index(){
        $benefits=MaintainedDeduction::all();
        return view('payroll.add',compact('benefits'));
    }

    public  function add(Request $request){
        $request->validate([
            'employee_id' => 'required',
        ]);
        if($request->employee_id=='0'){
            return redirect()->back()->with('error','Please Select employee');
        }

        if(!empty($request->name)) {
            $count = count($request->name);
            for ($i = 0; $i < $count; ++$i) {
                if (empty($request->name[$i])) {

                }
                $newDate = now()->addMonths($request->frequency[$i]);
                $amount_deducted=round(($request->amount[$i])/$request->frequency[$i],2);
                $data = array(
                    'employee_id' => $request->employee_id,
                    'name' => $request->name[$i],
                    'amount' => $request->amount[$i],
                    'frequency' => $request->frequency[$i],
                    'expiry' => $newDate,
                    'amount_deducted' => $amount_deducted,
                    'balance' => $request->amount[$i],
                    'deduction_id' => $request->deduction_id[$i],
                );
                Deduction::insert($data);
            }
        }

        return redirect()->route('payroll/deduction')->with('success','Deduction data updated successfully');
    }

public  function allDeduction(){
        $deductions=Deduction::all();
    $allowances=MaintainedDeduction::all();
        return view('payroll.deduction',compact('deductions','allowances'));
}
public  function allDeductionByUser(Request $request){
        if($request->id){
            $user=Employee::find($request->id);
            $deductions=Deduction::where('employee_id',$request->id)->get();
        }
        if($request->deduction_id){
            $deductions=Deduction::where('deduction_id',$request->deduction_id)->get();
        }
    $allowances=MaintainedDeduction::all();
        return view('payroll.deduction',compact('deductions','user','allowances'));
    }

public  function removeDeduction($id){
        $data=Deduction::findOrFail($id);
        $data->delete();
    return redirect()->back()->with('success','Deduction data removed successfully');

}
public  function updateDeduction(Request $request,$id){
        $data=Deduction::findOrFail($id);
        if($data->amount!==$data->balance){
            return redirect()->back()->with('error','The deducted cannot be updated, it is under payment process');
        }
        $data->update($request->all());
    return redirect()->back()->with('success','Deduction data updated successfully');
}

public  function deduction_logs($id){
 $logs=DeductionLog::where('deduction_id',$id)->get();

 return view('payroll.deduction_logs',compact('logs'));
}

}
