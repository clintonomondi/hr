<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Deduction;
use App\Employee;
use App\MaintainedAllowance;
use App\MaintainedDeduction;
use App\Payroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BenefitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   public  function index(){
       $benefits=MaintainedAllowance::all();
       return view('benefit.benefit',compact('benefits'));
   }

   public  function getBenefitData(Request $request){
       $benefit=MaintainedAllowance::find($request->id);
       return $benefit;
   }
   public function getDeductionData(Request $request){
       $benefit=MaintainedDeduction::find($request->id);
       return $benefit;
   }

   public  function add(Request $request){
       $request->validate([
           'employee_id' => 'required',
       ]);
       if($request->employee_id=='0'){
           return redirect()->back()->with('error','Please Select employee');
       }

       if(!empty($request->name)) {
           $count = count($request->name);
           for ($i = 0; $i < $count; ++$i) {
               if (empty($request->name[$i])) {

               }
                $check=Benefit::where('employee_id',$request->employee_id)->where('allowance_id',$request->allowance_id[$i])->where('months','!=','0')->first();
               if(!empty($check)){

               }else {
                   $data = array(
                       'employee_id' => $request->employee_id,
                       'name' => $request->name[$i],
                       'amount' => $request->amount[$i],
                       'frequency' => $request->frequency[$i],
                       'months' => $request->frequency[$i],
                       'date' => now(),
                       'user_id' => Auth::user()->id,
                       'allowance_id' => $request->allowance_id[$i],
                   );
                   Benefit::insert($data);
                   unset($data);
               }

           }

       }

       return redirect()->route('allowance/all')->with('success','Allowance data updated successfully');
   }

   public  function all(){
       $benefits=Benefit::orderBy('id','desc')->get();
       $allowances=MaintainedAllowance::all();
       return view('benefit.all',compact('benefits','allowances'));
   }
    public  function allDeductionByUser(Request $request){
       if($request->id) {
           $user = Employee::find($request->id);
           $benefits = Benefit::where('employee_id', $request->id)->get();
       }
       if($request->allowance_id){
           $benefits = Benefit::where('allowance_id', $request->allowance_id)->get();
       }
        $allowances=MaintainedAllowance::all();
        return view('benefit.all',compact('benefits','user','allowances'));
    }

   public  function remove($id){
       $data=Benefit::findorFail($id);
       $data->delete();
       return redirect()->back()->with('success','Allowance data removed successfully');
   }

   public  function update(Request $request,$id){
       $data=Benefit::findorFail($id);
       $data->update($request->all());
       return redirect()->back()->with('success','Allowance data updated successfully');
   }

   public  function maintained(){
      $allowances=MaintainedAllowance::all();
       return view('maintained.maintainedallowances',compact('allowances'));
   }

   public  function pushmaintained(Request $request){
       $request->validate([
           'name' => 'required',
       ]);
       $data=MaintainedAllowance::create($request->all());
       return redirect()->back()->with('success','Allowance data updated successfully');
   }

   public  function updatemaintained(Request $request,$id){
       $data=MaintainedAllowance::findorFail($id);
       $data->update($request->all());
       return redirect()->back()->with('success','Allowance data updated successfully');
   }

}
