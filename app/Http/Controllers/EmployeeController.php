<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Payroll;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function register(){
        return view('employee.register');
    }

    public  function postUser(Request $request){
        $validatedData = $request->validate([
            'fname' => 'required', 'lname' => 'required', 'gender' => 'required', 'phone' => 'required|:unique:employees|:unique:users|max:10|min:10', 'title' => 'required',
            'qualification' => 'required', 'type' => 'required', 'doj' => 'required',
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
            'nssf'=>'required|:unique:employees',
            'nhif'=>'required|:unique:employees',
            'idno'=>'required|:unique:employees',
            'gross'=>'required',
        ]);
        $regno='';
        $data='TCL';
        $count=Employee::count();
        if($count<1){
            $regno='1';
        }
        else {
            $regno = $count + 1;
        }
        $regno2=str_pad($regno,4,"0",STR_PAD_LEFT);
        $regno3=$data.''.$regno2;
        $request['regno']=$regno3;

        $member=Employee::create($request->all());
        $request['name']=$request->fname.'  '.$request->lname;
        $request['employee_id']=$member->id;
        $postuser=User::create($request->all());


//get file name with extension
        $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatar')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

        $request['employee_id']=$member->id;
        $request['avatars']=$fileNameToStore;
        $date=Photo::create($request->all());
        $gross=Payroll::create($request->all());

//        $message='Hello '.$request->fname.', this this is to notify that you have been registered in Trigals Limited HR system, your email is '.$request->email.' Please your ur email or this phone number to login: https://trigalsems.com/';
//        ExternalApiController::sendSMS($request->phone,$message);
         return ['status'=>true,'message'=>'Information submitted successfully, redirecting'];

    }

    public  function getActive(){
        $users=Employee::where('status','Active')->orderBy('id','desc')->get();
        return view('employee.active',compact('users'));
    }
    public  function getInctive(){
        $users=Employee::where('status','Inactive')->orderBy('id','desc')->get();
        return view('employee.inactive',compact('users'));
    }

    public  function edit($id){
        $user=Employee::findorfail($id);
        return view('employee.edit',compact('user'));
    }
    public  function profile($id){
        $user=Employee::findorfail($id);
        return view('employee.view',compact('user'));
    }

    public  function update(Request $request,$id){
        $data=Employee::find($id);
        $data->update($request->all());

        $user=User::where('employee_id',$id)->first();
        if(!empty($user)){
            $newuser=User::find($user->id);
            $request['name']=$request->fname.'  '.$request->lname;
            $newuser->update($request->all());
        }

        return redirect()->back()->with('success','Member Updated  successfully');

    }

    public  function updatePhoto(Request $request,$id){
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        $datas = DB::select( DB::raw("DELETE FROM photos WHERE employee_id='$id'") );

        //get file name with extension
        $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatar')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

        $request['avatars']=$fileNameToStore;
        $request['employee_id']=$id;
        $date=Photo::create($request->all());
        return redirect()->route('employee/active')->with('success','profile photo updated successfully');

    }

    public  function gallery(){
    $users=Employee::orderBy('id','desc')->paginate(8);
    return view('employee.gallery',compact('users'));
    }
}
