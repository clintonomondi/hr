<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deactivations extends Model
{
    protected  $fillable=['user_id','employee_id','reason','comment'];


    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }
}
