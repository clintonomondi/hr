<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected  $fillable=['fname','lname','email','marital','dob','gender','phone','idno','title','pin','nhif','nssf','qualification','emergency','relation','type',
        'doj','last_day','bank','account','status','regno',
        'prev_emp','prev_emp_from_date','prev_emp_to_date','huduma','kin','benef_name','benef_idno','benef_phone','benef_relation'];

    public  function photo(){
        return $this->hasOne(Photo::class);
    }

    public  function user(){
        return $this->hasOne(User::class);
    }

    public  function payroll(){
        return $this->hasOne(Payroll::class);
    }

    public  function leave(){
        return $this->hasMany(Leave::class);
    }

    public  function payslip(){
        return $this->hasMany(Payslip::class);
    }

    public  function benefit(){
        return $this->hasMany(Benefit::class);
    }
    public  function deduction(){
        return $this->hasMany(Deduction::class);
    }

    public  function deactivations(){
        return $this->hasMany(Deactivations::class);
    }
}
