<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NHIF extends Model
{
    protected  $fillable=['from_amount','to_amount','amount'];
}
