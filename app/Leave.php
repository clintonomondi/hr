<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected  $fillable=['leave_id','reason','start_date','days','end_date','status','comment','employee_id','created_by','updated_by','year','balance'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }

    public  function leave(){
        return $this->belongsTo(MaintainedLeave::class);
    }

}
