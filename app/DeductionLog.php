<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductionLog extends Model
{
    protected  $fillable=['employee_id','payslip_id','deduction_id','amount','balance','date','batch_id'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }
    public  function payslip(){
        return $this->belongsTo(Payslip::class);
    }

    public  function deduction(){
        return $this->belongsTo(Deduction::class);
    }
}
