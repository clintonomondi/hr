<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersionalRelief extends Model
{
    protected $fillable=['name','amount'];
}
