<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $fillable=['employee_id','gross'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }
}
