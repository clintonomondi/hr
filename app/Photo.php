<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected  $fillable=['employee_id','avatars'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }
}
