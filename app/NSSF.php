<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NSSF extends Model
{
    protected  $fillable=['from_amount','to_amount','amount'];
}
