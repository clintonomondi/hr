<?php

namespace App\Jobs;

use App\Mail\PayslipEmail;
use App\Payslip;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendPayslipEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected  $slips;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($slips)
    {
        $this->slips = $slips;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->slips as $slip) {
            $payslip = Payslip::find($slip->id);
            $data = array(
                'name' => $payslip->employee->fname . '  ' . $payslip->employee->lname,
                'payslip_no' => $payslip->payslip_no,
                'month' =>date('F', mktime(0, 0, 0, $payslip->month, 10)).'  ' .$payslip->year,
                'year' => $payslip->year,
                'login_link' => env('APP_URL') . '/login',
                'payslip_link' => env('APP_URL') . '/payslip/userview/' . $slip->id,
                'base_url' => 'http://hr.clintontest.com/',
            );

            Mail::to($payslip->employee->email)->send(new PayslipEmail($data));
        }
    }
}
