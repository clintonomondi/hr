<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    protected  $fillable=['kpi','unit','target','user_id'];

    public  function user(){
        return $this->belongsTo(User::class);
    }
}


