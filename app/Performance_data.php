<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance_data extends Model
{
    protected  $fillable=['performance_id','user_id','unit','week'];

    public  function performance(){
        return $this->belongsTo(Performance::class);
    }
}
