<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    protected  $fillable=['employee_id','kpi_id','status','from','to','target'];

    public  function kpi(){
        return $this->belongsTo(Kpi::class);
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public  function performance_data(){
        return $this->hasMany(Performance_data::class);
    }
}
