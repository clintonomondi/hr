<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    protected $fillable=['employee_id','name','amount','frequency','expiry','amount_deducted','balance','deduction_id'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }

    public  function deduction(){
        return $this->belongsTo(MaintainedDeduction::class);
    }
}
