<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $fillable=['employee_id','name','amount','date','user_id','allowance_id','frequency','months'];

    public  function employee(){
        return $this->belongsTo(Employee::class);
    }

    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function allowance(){
        return $this->belongsTo(MaintainedAllowance::class);
    }
}
