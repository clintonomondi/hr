<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paye extends Model
{
    protected  $fillable=['from_amount','to_amount','percentage'];
}
