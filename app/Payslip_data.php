<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payslip_data extends Model
{
    protected  $fillable=['payslip_id','type','amount','name','month','year','date','deduction_id','benefit_id','batch_id','employee_id'];

    public  function payslip(){
        return $this->belongsTo(Payslip::class);
    }

    public  function benefit(){
        return $this->belongsTo(MaintainedAllowance::class);
    }

    public  function deduction(){
        return $this->belongsTo(MaintainedDeduction::class);
    }


}
