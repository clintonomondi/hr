<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainedDeduction extends Model
{
    protected  $fillable=['name'];

    public  function deduction(){
        return $this->hasMany(Deduction::class);
    }

    public  function payslip_data(){
        return $this->hasMany(Payslip_data::class);
    }
}
