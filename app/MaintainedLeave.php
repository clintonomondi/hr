<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainedLeave extends Model
{
    protected  $fillable=['name','user_id','days'];

    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function leaves(){
        return $this->hasMany(Leave::class);
    }
}
