<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintainedAllowance extends Model
{
    protected  $fillable=['name'];

    public  function benefit(){
        return $this->$this->hasMany(Benefit::class);
    }

    public  function payslip_data(){
        return $this->hasMany(Payslip_data::class);
    }
}
