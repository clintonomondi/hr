
alertify.set('notifier','position', 'top-center');
function refreshPage(){
    window.location.reload();
}

function sendotp() {
    $('#submit').attr('disabled', true);
    $('#submit').text('Please wait..');
    var phone = document.getElementById('phone').value;

    if (phone === "") {
        alertify.error('Please enter a valid phone number');
        $('#submit').attr('disabled', false);
        $('#submit').text('Send Code');
    }else{
        var data;
        data = new FormData();
        data.append('phone',phone);
        $.ajax({
            url: "/password/generate/otp",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#submit').attr('disabled', false);
                $('#submit').text('Send Code');

                if(data.status){
                    alertify.success(data.message)
                    localStorage.setItem('name',data.info.name);
                    localStorage.setItem('phone',data.info.phone);
                    localStorage.setItem('email',data.info.email);
                    document.getElementById("name").innerHTML = data.info.name;
                    document.getElementById("phone2").innerHTML = data.info.phone;
                    document.getElementById("email").innerHTML = data.info.email;

                    $('#lowerdiv').hide();
                    $('#verify').modal('show');
                }else{
                    alertify.error(data.message)

                }
            },
            error:function(data) {
                $('#submit').attr('disabled', false);
                $('#submit').text('Send Code');
                alertify.error('System technical error,try again');
            }
        });


    }

}

function verifyCode() {
    $('#verifybtn').attr('disabled', true);
    $('#verifybtn').text('Please wait..');
    var code = document.getElementById('code').value;
        var data;
        data = new FormData();
        data.append('code',code);
        data.append('phone',localStorage.getItem('phone'));
        $.ajax({
            url: "/password/verify/otp",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#verifybtn').attr('disabled', false);
                $('#verifybtn').text('Verify Code');

                if(data.status){
                    alertify.success(data.message)
                    localStorage.setItem('code',data.code);
                    $('#lowerdiv').show();
                    $('#upperdiv').hide();

                }else{
                    alertify.error(data.message)
                }
            },
            error:function(data) {
                $('#verifybtn').attr('disabled', false);
                $('#verifybtn').text('Verify Code');
                alertify.error('System technical error,try again');
            }
        });
}

function setPassowrd() {
    $('#setpasswordbtn').attr('disabled', true);
    $('#setpasswordbtn').text('Please wait..');
    var password = document.getElementById('password').value;
    var repass = document.getElementById('repass').value;
    var data;
    data = new FormData();
    data.append('password',password);
    data.append('repass',repass);
    data.append('phone',localStorage.getItem('phone'));
    data.append('code',localStorage.getItem('code'));
    data.append('email',localStorage.getItem('email'));
    $.ajax({
        url: "/password/set",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('#setpasswordbtn').attr('disabled', false);
            $('#setpasswordbtn').text('Submit');

            if(data.status){
                alertify.success(data.message)
                window.location = '/login';
            }else{
                alertify.error(data.message)
            }
        },
        error:function(data) {
            $('#setpasswordbtn').attr('disabled', false);
            $('#setpasswordbtn').text('Submit');
            alertify.error('System technical error,try again');
        }
    });
}
