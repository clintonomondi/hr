
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

    $( "#selUser" ).select2({
        ajax: {
            url:"/apendusers",
            type: "GET",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }

    });

});


// var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

    $( "#report" ).select2({
        ajax: {
            url:"/getPayslipDate",
            type: "GET",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    _token: CSRF_TOKEN,
                    search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }

    });

});

function submitform() {
    document.forms["reportform"].submit();
}

function postGalarySearch() {
    document.forms["reportform"].submit();
}
