
$( document ).ready(function() {
    getData();

});

function getData() {
    $.ajax({
        url: "/getData",
        method: "GET",
        contentType: false,
        processData: false,
        catche: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            DrawDeductionlinechart(response);
            DrawGenderPieChart(response);
        }

    });
}




function DrawDeductionlinechart(response) {
    var months = [];
    var benefits = [];
    var deduction = [];
    $.each(response.benefit, function(i, item) {
        months.push(item.month);
        benefits.push(item.amount);
    });
    $.each(response.deduction, function(i, item) {
        deduction.push(item.amount);
    });
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: months,
            datasets: [{
                label: "ALLOWANCES",
                data: benefits,
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
                {
                    label: "DEDUCTIONS",
                    data: deduction,
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            responsive: true
        }
    });

}

function DrawGenderPieChart(response) {
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Male", "Female", "Others"],
            datasets: [{
                data: [response.gender.male, response.gender.female, response.gender.other],
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C"],
                hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870"]
            }]
        },
        options: {
            responsive: true
        }
    });
}
