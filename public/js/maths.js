// var tds = document.getElementById('table3').getElementsByTagName('td');
// var currentsum = 0;
// var ytdsum = 0;
// for(var i = 0; i < tds.length; i ++) {
//     if(tds[i].className == 'earningCurrent') {
//         currentsum += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
//     }
//     if(tds[i].className == 'earningYTD') {
//         ytdsum += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
//     }
// }
// document.getElementById('earningCurrentTotal').innerHTML = "KSH    " +currentsum;
// document.getElementById('earningYTDTotal').innerHTML = "KSH    " +ytdsum;
//
//
// var tds = document.getElementById('table2').getElementsByTagName('td');
// var deductionTotal = 0;
//  var ytddedsum = 0;
// for(var i = 0; i < tds.length; i ++) {
//     if(tds[i].className == 'deductionCureent') {
//         deductionTotal += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
//     }
//     if(tds[i].className == 'deductionytd') {
//         ytddedsum += isNaN(tds[i].innerHTML) ? 0 : parseFloat(tds[i].innerHTML);
//     }
// }
// document.getElementById('totalDeduction').innerHTML = "KSH    " +deductionTotal;
//  document.getElementById('deductiontotalYtd').innerHTML = "KSH    " +ytddedsum;
//
//  var netincome=currentsum-deductionTotal;
//  var ytdincome=ytdsum-ytddedsum;
// document.getElementById('netincome').innerHTML = "KSH    " +netincome;
// document.getElementById('ytdincome').innerHTML = "KSH    " +ytdincome;





function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
var days = document.getElementById("days").innerHTML;
var deadline = new Date(Date.parse(new Date()) + days * 24 * 60 * 60 * 1000);
initializeClock('clockdiv', deadline);



$(function () {
    $('#datetimepicker1').datetimepicker({
        viewMode: 'years',
        format: 'MM/YYYY'
    });

});
