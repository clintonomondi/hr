alertify.set('notifier','position', 'top-center');
function refreshPage(){
    window.location.reload();
}

$( document ).ready(function() {
    register();
});



function validateForm() {
    var fname = document.forms["myForm"]["fname"].value;
    var lname = document.forms["myForm"]["lname"].value;
    var phone = document.forms["myForm"]["phone"].value;
    var gender = document.forms["myForm"]["gender"].value;
    var title = document.forms["myForm"]["title"].value;
    var qualification = document.forms["myForm"]["qualification"].value;
    var type = document.forms["myForm"]["type"].value;
    var doj = document.forms["myForm"]["doj"].value;
    var nhif = document.forms["myForm"]["nhif"].value;
    var nssf = document.forms["myForm"]["nssf"].value;
    var idno = document.forms["myForm"]["idno"].value;
    var gross = document.forms["myForm"]["gross"].value;
    if (fname === "") {
        alertify.error('Enter first name');
        return false;
    }
    if (gross <=0) {
        alertify.error('Basic Salary Cannot be zero');
        return false;
    }
    if (lname === "") {
        alertify.error('Enter Last name');
        return false;
    }
    if (fname === "") {
        alertify.error('Enter first name');
        return false;
    }
    if (phone === "") {
        alertify.error('Enter Phone number');
        return false;
    }
    if (gender === "") {
        alertify.error('Select gender');
        return false;
    }
    if (title === "") {
        alertify.error('Enter Title of the employee');
        return false;
    }
    if (qualification === "") {
        alertify.error('Select qualification');
        return false;
    }
    if (type === "") {
        alertify.error('Select type of contarct');
        return false;
    }
    if (doj === "") {
        alertify.error('Enter date of registration');
        return false;
    }
    if (nssf === "") {
        alertify.error('Enter NSSF');
        return false;
    }
    if (nhif === "") {
        alertify.error('Enter NHIF');
        return false;
    }
    if (idno === "") {
        alertify.error('Enter ID Number');
        return false;
    }
    if(phone.length!==10){
        alertify.error('Phone number must be 10 characters');
        return false;
    }
}

function validatePayrollForm() {
    var gross = document.forms["myForm"]["gross"].value;
    var id = document.forms["myForm"]["employee_id"].value;

    if (gross<= "0") {
        alertify.error('Basic Salary Amount cannot be zero or less');
        return false;
    }

    if (id === "") {
        alertify.error('Please select employee');
        return false;
    }
}

// function validateLeaveForm() {
//     var start = document.forms["myForm"]["start_date"].value;
//     var end = document.forms["myForm"]["end_date"].value;
//     if (start===end) {
//         alertify.error('Start date and Ending date cannot be equal');
//         return false;
//     }
//
// }

function calculateMonthly(itemIndex) {
    var amount=document.getElementById('amount'+itemIndex).value;
    var frequency=document.getElementById('frequency'+itemIndex).value;
    var monthly=amount/frequency;
    document.getElementById('monthly'+itemIndex).innerHTML='Monthly KSh. '+monthly.toFixed(2);
    document.getElementById('balance'+itemIndex).value=monthly.toFixed(2);


}
function calculateMonthly2(itemIndex) {
    var amount=document.getElementById('amount'+itemIndex).value;
    var frequency=document.getElementById('frequency'+itemIndex).value;
    var monthly=amount/frequency;
     document.getElementById('monthly'+itemIndex).innerHTML='Monthly KSh. '+monthly.toFixed(2);
     var val=monthly.toFixed(2);
    document.getElementById('balance'+itemIndex).value=amount;
    document.getElementById('amount_deducted'+itemIndex).value=val;


}

function updateRole(role,id) {
    var data;
    data = new FormData();
    data.append('id', id);
    data.append('role', role);
    $.ajax({
        url: "/updateRole",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if(data.status){
                alertify.success(data.message);
                refreshPage();
            }else{
                alertify.error(data.message);
            }
        },
        error:function(data) {
            alertify.error('System technical error');
        }
    });

}
function updateStatus(status,id) {
    $('#modal2'+id).modal('hide');
    $('body').preloader({text: 'Please Wait...!', });
    var data;
    data = new FormData();
    data.append('id', id);
    data.append('status', status);
    $.ajax({
        url: "/updateStatus",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('body').preloader('remove');
            if(data.status){
                alertify.success(data.message);
                refreshPage();
            }else{
                alertify.error(data.message);
            }
        },
        error:function(data) {
            $('body').preloader('remove');
            alertify.error('System technical error');
        }
    });

}


function approvePayslips(){
    $('#approve').modal('hide');
    $(document).on('submit', '#approveForm', function(event){
        event.preventDefault();
        $('body').preloader({text: 'Please Wait...!', });
        var data;
        data = new FormData(this);
        $.ajax({
            url: "/payslip/approve",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('body').preloader('remove');
                if(data.status){
                    alertify.success(data.message)
                    window.location = '/payslip/index';
                }else{
                alertify.error(data.message)
                }
            },
            error:function(data) {
                $('body').preloader('remove');
                alertify.error('System technical error');
            }
        });
    });
}
function rejectPayslips(){
    var remarks=$('#comment').val();
    $('#reject').modal('hide');
    $(document).on('submit', '#approveForm', function(event){
        event.preventDefault();
        $('body').preloader({text: 'Please Wait...!', });
        var data;
        data = new FormData(this);
        data.append('remarks',remarks);
        data.append('status','rejected');
        $.ajax({
            url: "/payslip/reject",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('body').preloader('remove');
                if(data.status){
                    alertify.success(data.message)
                    window.location = '/payslip/index';
                }else{
                    alertify.error(data.message)
                }
            },
            error:function(data) {
                $('body').preloader('remove');
                alertify.error('System technical error');
            }
        });
    });
}

function Dispatch(batch_id) {
        $('body').preloader({text: 'Please Wait...!', });
        var data;
        data = new FormData();
        data.append('batch_id',batch_id);
        $.ajax({
            url: "/sendPayslipEmail",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('body').preloader('remove');
                if(data.status){
                    alertify.success(data.message)
                }else{
                    alertify.error(data.message)
                }
            },
            error:function(data) {
                $('body').preloader('remove');
                alertify.error('System technical error,try gain');
            }
        });

}

function register() {
    $(document).on('submit', '#submitKasae', function(event){
        event.preventDefault();
    $('body').preloader({text: 'Please Wait...!', });
    var data;
    data = new FormData(this);
    $.ajax({
        url: "/employee/register",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('body').preloader('remove');
            if(data.status){
                alertify.success(data.message);
                setTimeout(function() {
                    location.href = '/employee/active';
                }, 3000);
            }else{
                alertify.error(data.message)
            }
        },
        error:function(error) {
            $('body').preloader('remove');
            alertify.error(error.responseText);
        }
    });
    });

}

