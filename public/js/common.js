
$( document ).ready(function() {
    getLeaves();
    register();
});

$("#selectAll").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});

function refreshPage(){
    window.location.reload();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)

        };

        reader.readAsDataURL(input.files[0]);
    }
}

// function getIndividualPayrolldata(id) {
//     $("tr:has(td)").remove();
//     $('body').preloader();
//     var data;
//     data = new FormData();
//     data.append('id', id.value);
//     $.ajax({
//         url: "/getIndividualPayrolldata",
//         // url:"http://192.168.3.11/church/public/getLineChartData",
//         method: "POST",
//         contentType: false,
//         processData: false,
//         catche: false,
//         data: data,
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         success: function (data) {
//             $('body').preloader('remove');
//             $("#gross").val(data.gross.gross);
//             $.each(data.deductions, function(i, item) {
//                 var $tr = $('<tr>').append(
//                     $('<td>').text(item.name),
//                     $('<td>').text(item.amount),
//                     $('<td>').text(item.frequency),
//                     $('<td>').text(item.amount_deducted),
//                     $('<td>').text(item.balance),
//                     $('<td>').text(item.expiry),
//                 ).appendTo('#deductiontable');
//             });
//
//
//         },
//         error:function(data) {
//             $('body').preloader('remove');
//         }
//
//     });
// }
// function getIndividualPayrolldata2(id) {
//     $("tr:has(td)").remove();
//     $('body').preloader();
//     var data;
//     data = new FormData();
//     data.append('id', id.value);
//     $.ajax({
//         url: "/getIndividualPayrolldata2",
//         // url:"http://192.168.3.11/church/public/getLineChartData",
//         method: "POST",
//         contentType: false,
//         processData: false,
//         catche: false,
//         data: data,
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         success: function (data) {
//             $('body').preloader('remove');
//             $("#gross").val(data.gross.gross);
//             $.each(data.benefit, function(i, item) {
//                 var $tr = $('<tr>').append(
//                     $('<td>').text(item.name),
//                     $('<td>').text(item.amount),
//                 ).appendTo('#deductiontable');
//             });
//
//
//         },
//         error:function(data) {
//             $('body').preloader('remove');
//         }
//
//     });
// }

function getLeaveInfo(id) {
    var data;
    data = new FormData();
    data.append('id', id.value);
    $.ajax({
        url: "/getLeaveInfo",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            document.getElementById("days").value = data.days;

        },
        error:function(data) {
            alertify.error('System error, cannot query data')
        }
    });
}

    function checkifuserhasleave(id) {

        var data;
        data = new FormData();
        data.append('employee_id', id.value);
        $.ajax({
            url: "/checkifuserhasleave",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
              if(data>0){
                  $('#leavediv').empty();
                  $('#leavediv').append(`
                    <div class="alert alert-danger">
                                    <button class="close" data-dismiss="alert">&times;</button>
                                    <h4>Warning!</h4>
                                    <p>Sorry the employee is  illigible to apply for any leave.</p>
                                    <p>They have ${data} under request or on leave.</p>
                                </div>
                `
                  );
                  document.getElementById("myButton").disabled = true;
              }else{
                  $('#leavediv').empty();
                  $('#leavediv').append(`
                    <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">&times;</button>
                                    <h4>Success!</h4>
                                    <p>The employee is  ligible to apply for another leave.</p>
                                </div>
                `
                  );
                  document.getElementById("myButton").disabled = false;
              }
            },
            error:function(data) {
               alertify.error('System error, cannot query data')
            }
        });

    }

    function getLeaves() {
        $.ajax({
            url: "/getLeaves",
            // url:"http://192.168.3.11/church/public/getLineChartData",
            method: "GET",
            contentType: false,
            processData: false,
            catche: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#leave").text(response.leave);
                $("#leave2").text(response.leave);

                $.each(response.leaves, function (k) {
                    $('#leavelist').append(`
                   <li class="notify">
                        <a href="#">
                            <i class="fa fa-comment orange"></i>
                            <p>${response.leaves[k].type}</p>
                            <span class="badge badge-warning">${response.leaves[k].created_at}</span>
                        </a>
                    </li>
                `
                    );

                });

            }
        });
    }
    function addPayerrange() {
        $('#payerform').append(`
                   <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount from (Ksh)</label>
                                    <input class="form-control" name="from_amount[]" type="number"   required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount to (Ksh)</label>
                                    <input class="form-control" name="to_amount[]" type="number"    required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Percentage (%)</label>
                                    <input class="form-control" name="percentage[]" type="number"    required>
                                </div>
                            </div>
                             <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">----------------------</label>
                                            <a class="btn btn-danger btn-sm" href="#" onclick="refreshPage()"><i class="fa fa-ban">Remove</i></a>
                                        </div>
                                    </div>
                        </div>
                `
        );
    }
    function addnHIFrange() {
        $('#payerform').append(`
                   <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount from (Ksh)</label>
                                    <input class="form-control" name="from_amount[]" type="number"   required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount to (Ksh)</label>
                                    <input class="form-control" name="to_amount[]" type="number"    required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount</label>
                                    <input class="form-control" name="amount[]" type="number"    required>
                                </div>
                            </div>
                             <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">----------------------</label>
                                            <a class="btn btn-danger btn-sm" href="#" onclick="refreshPage()"><i class="fa fa-ban">Remove</i></a>
                                        </div>
                                    </div>
                        </div>
                `
        );
    }
    function addNssfrange() {
        $('#payerform').append(`
                   <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount from (Ksh)</label>
                                    <input class="form-control" name="from_amount[]" type="number"   required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount to (Ksh)</label>
                                    <input class="form-control" name="to_amount[]" type="number"    required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Amount</label>
                                    <input class="form-control" name="amount[]" type="number"    required>
                                </div>
                            </div>
                             <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">----------------------</label>
                                            <a class="btn btn-danger btn-sm" href="#" onclick="refreshPage()"><i class="fa fa-ban">Remove</i></a>
                                        </div>
                                    </div>
                        </div>
                `
        );
    }
var itemIndex=0;
function addDeduction(id) {
         itemIndex=itemIndex+1;
        var data;
        data = new FormData();
        data.append('id', id);
        $.ajax({
            url: "/getDeductionData",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#deductionForm').append(
                    `
                    <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input class="form-control" name="name[]" type="text" value="${data.name}"   required>
                                            <input hidden  name="deduction_id[]" type="text" value="${data.id}"    required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Total Amount (Ksh)</label>
                                            <input class="form-control" name="amount[]" type="number" id="amount${itemIndex}" value="0"  onkeyup="calculateMonthly(itemIndex)"   required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Frequency(Months)</label>
                                            <input class="form-control" name="frequency[]" type="number" id="frequency${itemIndex}" value="1"  onkeyup="calculateMonthly(itemIndex)"   required>
                                            <span style="color: blue;"  id="monthly${itemIndex}"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">-------------------------------------------------------------</label>
                                            <a class="btn btn-danger btn-sm" href="#" onclick="refreshPage()"><i class="fa fa-ban">Remove</i></a>
                                        </div>
                                    </div>
                                </div>
                `
                );
            }
        });


    }
function addBenefit(id) {
    var data;
    data = new FormData();
    data.append('id', id);
    $.ajax({
        url: "/getBenefitData",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('#deductionForm').append(`
                    <div class="row justify-content-center">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input class="form-control" name="name[]" type="text" value="${data.name}"   required>
                                            <input hidden  name="allowance_id[]" type="text" value="${data.id}"    required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Amount (Ksh)</label>
                                            <input class="form-control" name="amount[]" type="number"    required>
                                        </div>
                                    </div>
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Frequency (Number of Months)</label>
                                            <input class="form-control" name="frequency[]" type="number"    required>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">-----------------------------------------------------------------</label>
                                            <a class="btn btn-danger btn-sm" href="#" onclick="refreshPage()"><i class="fa fa-ban">Remove</i></a>
                                        </div>
                                    </div>
                                </div>
                `
            );
        }
    });

}

function changeLeave(id,status,comment) {
    $('body').preloader({text: 'Please Wait...!', });
    var data;
    data = new FormData();
    data.append('id', id);
    data.append('status', status);
    data.append('comment', comment);
    $.ajax({
        url: "/changeLeave",
        // url:"http://192.168.3.11/church/public/getLineChartData",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('body').preloader('remove');
            if(data.status){
                alertify.success(data.message);
                refreshPage();
            }
        },
        error:function(data) {
            $('body').preloader('remove');
            alertify.error('System technical error');
        }
    });

}


// function  showImage(id) {
//     // Get the modal
//     var modal = document.getElementById("myModal");
//
// // Get the image and insert it inside the modal - use its "alt" text as a caption
//     var img = document.getElementById(id);
//     var modalImg = document.getElementById("img01");
//     var captionText = document.getElementById("caption");
//     // img.onclick = function(){
//         modal.style.display = "block";
//         modalImg.src = img.src;
//         captionText.innerHTML = img.alt;
//     // }
//
// // Get the <span> element that closes the modal
//     var span = document.getElementsByClassName("close")[0];
//
// // When the user clicks on <span> (x), close the modal
//     span.onclick = function() {
//         modal.style.display = "none";
//     }
// }

function loadbutton() {
    $('#submit').attr('disabled', true);
    $('#submit').text('Please wait..');
}
