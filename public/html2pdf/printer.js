function generatePDF(payslip) {
    // Choose the element that our invoice is rendered in.
    const element = document.getElementById("printElement");
    // Choose the element and save the PDF for our user.
    html2pdf()
        .set({ html2canvas: { scale: 4 } })
        .from(element)
        .save(payslip+'.pdf');
}

